XBlog
=====


Copyright 2011 Keith Strickland
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License



Links to sites the plugins came from:

SyntaxHighlighter 3.0 - http://alexgorbatchev.com/SyntaxHighlighter/ - MIT License

Akismet-Java - http://sourceforge.net/projects/akismet-java/ - BSD License

REMOVED - OWASP AntiSamy - https://www.owasp.org/index.php/Category:OWASP_AntiSamy_Project - BSD license

REMOVED - Feed generator XPage agent - Thomas Ladehoff - http://www.openntf.org/internal/home.nsf/project.xsp?action=openDocument&name=Feed%20generator%20XPage%20agent - Apache License

XPages Dojo Login Custom Control - Ferry Kranenburg - http://www.openntf.org/internal/home.nsf/project.xsp?action=openDocument&name=Xpages%20Dojo%20Login%20Custom%20Control - Apache License

XPages Debug Toolbar - Mark Leusink - http://www.openntf.org/internal/home.nsf/project.xsp?action=openDocument&name=XPage%20Debug%20Toolbar - Apache License

DataProvider Bean - Code and idea by Karsten Lehmann - Permission to use the DataProvider bean given on 10/18/2011 via Email - http://www.mindoo.com/web/blog.nsf/dx/18.03.2011104725KLEDH8.htm?opendocument

JSFUtils Class - Code by Karsten Lehmann - Permission to use the JSFUtils Class given on 10/18/2011 via Email

CodeMirror - http://codemirror.net - MIT Style License (NOTE: No other licensed subdirectories are included in this implementation, it is a base implementation of code mirror with no plugins installed)

Sencha Touch - http://www.sencha.com - Distributed under GPL3 with the Exception for Applications - http://www.sencha.com/legal/open-source-faq/open-source-license-exception-for-applications. In this project, all components with the exception of Sencha Touch, are distributed under their original open source licenses (Apache, etc.) and not under GPL3 as per the terms of the Exception for Applications.