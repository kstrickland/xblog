Ext.regModel('DownloadsModel', {
	fields: [
	         {name: 'title', type:'string'},
	         {name: 'createdDate', type: 'string'},
	         {name: 'content', type: 'string'},
	         {name: 'attachmentHtml', type: 'string'},
	         {name: 'unid', type: 'string'}
	]
});