Ext.regModel('CommentModel', {
	fields: [
	         {name: 'title', type:'string'},
	         {name: 'createdDate', type: 'string'},
	         {name: 'content', type: 'string'},
	         {name: 'author', type: 'string'},
	         {name: 'email', type: 'string'},
	         {name: 'website', type: 'string'},
	         {name: 'parentUnid', type: 'string'},
	         {name: 'unid', type: 'string'}
	]
});