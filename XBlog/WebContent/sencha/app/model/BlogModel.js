Ext.regModel('BlogModel', {
	fields: [
	         {name: 'title', type:'string'},
	         {name: 'createdDate', type: 'string'},
	         {name: 'content', type: 'string'},
	         {name: 'unid', type: 'string'},
	         {name: 'allowComments', type: 'string'},
	         {name: 'postCommentButtonText', type: 'string'},
	         {name: 'commentText', type: 'string'},
	         {name: 'commentSave', type: 'string'},
	         {name: 'commentCancel', type: 'string'},
	         {name: 'allowComments', type: 'boolean'}
	]
});