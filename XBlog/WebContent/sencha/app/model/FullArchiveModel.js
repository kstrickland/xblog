Ext.regModel('FullArchiveModel', {
	fields: [
	         {name: 'title', type:'string'},
	         {name: 'createdDate', type: 'string'},
	         {name: 'content', type: 'string'},
	         {name: 'unid', type: 'string'},
	         {name: 'category', type: 'string'}
	]
});