package com.xblog.utils.rss;

/*
 * This class does the actual import and creation of documents from the RSSFeed. 
 */

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.imageio.ImageIO;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.MIMEEntity;
import lotus.domino.MIMEHeader;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.Stream;
import lotus.domino.View;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogUtils;

public class RSSImporter {

	public RSSImporter() {
	}

	/**
	 * Take an RSSPost and import it as a Post document
	 * 
	 * @param post
	 *            RSSPost - The post we're importing
	 */
	public static void importRSSPost(RSSPost post) {
		try {
			System.out.println("Importing Post " + post.getTitle());
			Database db = JSFUtil.getCurrentDatabase();
			Document doc = db.createDocument();
			doc.replaceItemValue("form", "Post");
			doc.replaceItemValue("title", post.getTitle());
			doc.replaceItemValue("author", post.getAuthor());

			String postText = post.getContentText();
			for (RSSImage image : post.getImages()) {
				RSSImage img = importRSSImage(image);
				if (postText != null && !postText.isEmpty()) {
					postText = postText.replace(img.getOrigUrl(), img.getNewPermalink());
				}
			}

			NotesContext.getCurrent().getCurrentSession().setConvertMime(false);
			if (postText == null || postText.isEmpty()) {
				postText = "<b>No Content for Post???</b>";
			}
			Stream stream = NotesContext.getCurrent().getCurrentSession().createStream();
			stream.writeText(postText);
			MIMEEntity postContent = doc.createMIMEEntity("postContent");
			postContent.setContentFromText(stream, "text/html;charset=UTF-8", postContent.ENC_IDENTITY_7BIT);
			NotesContext.getCurrent().getCurrentSession().setConvertMime(true);
			doc.closeMIMEEntities();

			SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
			if (post.getPubDate().indexOf("AM") > -1 || post.getPubDate().indexOf("PM") > -1) {
				dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss a Z");
			}
			Date javaDate = dateFormat.parse(post.getPubDate());
			DateTime createdNDate = NotesContext.getCurrent().getCurrentSession().createDateTime(javaDate);
			doc.replaceItemValue("createdDate", createdNDate);
			Vector tags = new Vector(post.getCategory());
			doc.replaceItemValue("tags", tags);
			doc.replaceItemValue("publish", "Yes");
			doc.replaceItemValue("OldPermalink", post.getGuid().substring(post.getGuid().lastIndexOf("/") + 1));
			doc.save(true, false);
			String permalink = XBlogUtils.getCurrentInstance().getRelativeUrl();
			doc.replaceItemValue("permalink", permalink + "/default.xsp?documentId=" + doc.getUniversalID());
			doc.save();
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Take an RSSComment and import it as a comment document
	 * 
	 * @param comment
	 *            RSSComment
	 */
	public static void importRSSComment(RSSComment comment) {
		System.out.println("Importing Comment " + comment.getTitle());
		try {
			Database db = JSFUtil.getCurrentDatabase();
			String startGuid = comment.getGuid();
			if (startGuid == null || startGuid.isEmpty()) {
				startGuid = comment.getLink();
			}
			if (startGuid.startsWith("http://")) {
				String rightBackGuid = startGuid.substring(startGuid.lastIndexOf("/") + 1);
				String guid = "";
				if (rightBackGuid.indexOf("#comment-") > -1) {
					guid = rightBackGuid.substring(0, rightBackGuid.lastIndexOf("#comment-"));
				} else if (rightBackGuid.indexOf("?opendocument") > -1) {
					guid = rightBackGuid.substring(0, rightBackGuid.lastIndexOf("?opendocument"));
				}
				Document parentDoc = getParentDoc(guid);
				if (parentDoc != null) {
					Document commentDoc = db.createDocument();
					commentDoc.replaceItemValue("form", "Comment");
					if (comment.getTitle().equalsIgnoreCase("comment")) {
						commentDoc.replaceItemValue("Subject", "Re: " + parentDoc.getItemValueString("Title"));
					} else {
						commentDoc.replaceItemValue("Subject", comment.getTitle());
					}
					commentDoc.replaceItemValue("Name", comment.getAuthor());
					SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
					if (comment.getPubDate().indexOf("AM") > -1 || comment.getPubDate().indexOf("PM") > -1) {
						dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss a Z");
					}
					Date javaDate = dateFormat.parse(comment.getPubDate());
					DateTime createdNDate = NotesContext.getCurrent().getCurrentSession().createDateTime(javaDate);
					commentDoc.replaceItemValue("CreatedDate", createdNDate);
					if (comment.getLink().startsWith("http://")) {
						commentDoc.replaceItemValue("Website", comment.getLink());
					} else {
						commentDoc.replaceItemValue("Website", "http://" + comment.getLink());
					}
					commentDoc.replaceItemValue("Comment", comment.getContentText());
					commentDoc.replaceItemValue("PendingApproval", "No");
					commentDoc.replaceItemValue("Blocked", "No");
					commentDoc.replaceItemValue("NotifyMe", "No");
					commentDoc.replaceItemValue("ParentUNID", parentDoc.getUniversalID());
					commentDoc.replaceItemValue("OldPermalink", guid);
					commentDoc.makeResponse(parentDoc);
					commentDoc.save(true, false);
					int commentNum = parentDoc.getResponses().getCount();
					String permalink = XBlogUtils.getCurrentInstance().getRelativeUrl();
					commentDoc.replaceItemValue("Permalink", permalink + "/default.xsp?documentId=" + parentDoc.getUniversalID() + "#"
							+ commentNum);
					parentDoc.replaceItemValue("numberComments", commentNum);
					commentDoc.replaceItemValue("CommentNumber", commentNum);
					parentDoc.save();
					commentDoc.save();
				} else {
					System.out.println("Could not find parent document with GUID of " + guid);
				}
			}
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get a comment's parent document by the old GUID of the parent
	 * 
	 * @param identifier
	 *            - String - The id of the original post (in blogsphere the id
	 *            is something like KSTD-####)
	 * @return Document - The parent document that has the same blogsphere id as
	 *         the passed identifier
	 */
	private static Document getParentDoc(String identifier) {
		View parentView;
		try {
			parentView = JSFUtil.getCurrentDatabase().getView("(luOldPermalink)");
			Document parentDoc = parentView.getDocumentByKey(identifier);
			if (parentDoc != null) {
				return parentDoc;
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Import the images found during the creation of the feed
	 * 
	 * @param image
	 *            - RSSImage
	 */
	public static RSSImage importRSSImage(RSSImage image) {
		System.out.println("Importing Image " + image.getFileName());
		Session sess = NotesContext.getCurrent().getCurrentSession();
		Database db = JSFUtil.getCurrentDatabase();
		Document imageDoc;
		String fullUrl = XBlogUtils.getCurrentInstance().getBlogUrl();
		try {
			imageDoc = db.createDocument();
			imageDoc.replaceItemValue("Form", "Image");
			imageDoc.save(true, false);
			String unid = imageDoc.getUniversalID();
			imageDoc.replaceItemValue("Name", image.getFileName());
			String url = fullUrl + "/0/" + unid + "/$FILE/" + image.getFileName();
			imageDoc.replaceItemValue("URL", url);
			image.setNewPermalink(url);
			imageDoc.replaceItemValue("IMGTag", "<img src='" + url + "' alt='" + image.getFileName() + "'>");

			sess.setConvertMime(false);
			attachImage(imageDoc, image);
			sess.setConvertMime(true);
			imageDoc.closeMIMEEntities();

			imageDoc.save();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return image;
	}

	private static void attachImage(Document imageDoc, RSSImage image) {
		try {
			MIMEEntity mime = imageDoc.createMIMEEntity("ImageFile");
			MIMEHeader header = mime.createHeader("Content-Disposition");
			header.setHeaderVal("attachment; filename=\"" + image.getFileName() + "\"");
			header = mime.createHeader("Content-ID");
			header.setHeaderVal(image.getFileName());

			BufferedImage buffImg = image.getBaseImg();
			byte[] imgByteArray = getImgByteArray(buffImg, image.getExtension());
			InputStream is = new ByteArrayInputStream(imgByteArray);
			Stream stream = NotesContext.getCurrent().getCurrentSession().createStream();
			stream.setContents(is);
			mime.setContentFromBytes(stream, "image/" + image.getExtension(), mime.ENC_IDENTITY_BINARY);
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	private static byte[] getImgByteArray(BufferedImage buffImg, String imageExtension) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(buffImg, imageExtension, baos);
			byte[] imgByteArray = baos.toByteArray();
			return imgByteArray;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
