package com.xblog.utils;

/*
 * The applicationScope bean
 */

import java.util.HashMap;
import java.util.Map;

public class XBlogAppScope {

	private static final long serialVersionUID = 1L;
	private static Map<String, Integer> nowViewing = new HashMap<String, Integer>();

	public XBlogAppScope() {
		if (nowViewing == null) {
			nowViewing = new HashMap<String, Integer>();
		}
	}

	public static XBlogAppScope getCurrentInstance() {
		if (JSFUtil.getVariableValue("XBlogAppScope") instanceof XBlogAppScope) {
			return (XBlogAppScope) JSFUtil.getVariableValue("XBlogAppScope");
		} else {
			return null;
		}
	}

	public static void addNowViewing(String unid) {
		if (nowViewing.containsKey(unid)) {
			nowViewing.put(unid, nowViewing.get(unid) + 1);
		} else {
			nowViewing.put(unid, 1);
		}
	}

	public static Map<String, Integer> getNowViewing() {
		return nowViewing;
	}

	public static void removeNowViewing(String unid) {
		if (nowViewing.containsKey(unid)) {
			if (nowViewing.get(unid) <= 1) {
				nowViewing.remove(unid);
			} else {
				nowViewing.put(unid, nowViewing.get(unid) - 1);
			}
		}
	}

	public static boolean isBeingMaintained() {
		if (XBlogUtils.getCurrentInstance().getConfigValue("SiteMaintainenance", 0).toString().equalsIgnoreCase("Yes")) {
			return true;
		} else {
			return false;
		}
	}
}
