package com.xblog.utils;

/**
 * This class is for adding dojo module resources to pages where needed. The only resources
 * listed here are those that are not included with the Extension Library
 */

import com.ibm.xsp.extlib.resources.ExtLibResources;
import com.ibm.xsp.resource.DojoModuleResource;
import com.ibm.xsp.resource.StyleSheetResource;

public class XBlogResources extends ExtLibResources {

	public static final DojoModuleResource dojoTitlePane = new DojoModuleResource("dijit.TitlePane");
	public static final DojoModuleResource dojoxFeedPortlet = new DojoModuleResource("dojox.widget.FeedPortlet");
	public static final DojoModuleResource dojoxPortlet = new DojoModuleResource("dojox.widget.Portlet");
	public static final StyleSheetResource dojoxPortletCss = new StyleSheetResource("/.ibmxspres/dojoroot/dojox/widget/Portlet/Portlet.css");
	public static final StyleSheetResource dojoxWizardCss = new StyleSheetResource("/.ibmxspres/dojoroot/dojox/widget/Wizard/Wizard.css");

}
