/*
 * <<
 * XPage Debug Toolbar
 * Copyright 2012 Mark Leusink
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
 * file except in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
 * ANY KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License
 * >> 
 */

package com.xblog.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.ibm.xsp.extlib.util.json.JObject;

@SuppressWarnings("unchecked")
public class DebugToolbar {

	private static final String TYPE_ERROR = "error";
	private static final String TYPE_INFO = "info";
	private static final String TYPE_DEBUG = "debug";
	private static final String TYPE_WARNING = "warning";
	private static final int MAX_MESSAGES = 1000;
	private static Map<Object, Object> sessionScope = (Map<Object, Object>) JSFUtil.getVariableValue("sessionScope");
	private Boolean loaded = false;
	private HashMap<String, Object> toolbarConfig = null;

	public DebugToolbar() {

		if (toolbarConfig == null) {
			getToolbarConfig();
		}

	}

	/*
	 * log a message to the toolbar
	 */
	public void log(String msg, String msgContext, String type) {

		HashMap<String, Object> conf = getToolbarConfig();

		boolean loaded = (Boolean) conf.get("loaded");

		if (conf != null && loaded) {

			List<JObject> msgs = (List<JObject>) conf.get("logMessages");

			JObject newMsg = new JObject();
			SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
			newMsg.putString("text", msg);
			newMsg.putString("type", type);
			newMsg.putString("date", format.format(new Date()));
			newMsg.putString("msgContext", msgContext);
			msgs.add(0, newMsg);

			if (msgs.size() > MAX_MESSAGES) {
				msgs.remove(msgs.size() - 1);
			}

			conf.put("logMessages", msgs);

			this.setToolbarConfig(conf);
		}
	}

	public void info(String msg, String msgContext) {
		log(msg, msgContext, TYPE_INFO);
	}

	public void debug(String msg, String msgContext) {
		log(msg, msgContext, TYPE_DEBUG);
	}

	public void error(String msg, String msgContext) {
		log(msg, msgContext, TYPE_ERROR);
	}

	public void warn(String msg, String msgContext) {
		log(msg, msgContext, TYPE_WARNING);
	}

	public Boolean isLoaded() {
		if (XBlogUtils.getCurrentInstance().getConfigValue("Debug", 0).toString().equalsIgnoreCase("Yes")) {
			loaded = true;
		}
		return loaded;
	}

	public void setLoaded(Boolean loaded) {
		this.loaded = loaded;
	}

	/*
	 * returns current config or creates new (default) config
	 */
	public HashMap getToolbarConfig() {

		if (toolbarConfig != null) {
			return toolbarConfig;
		} else {
			HashMap toolbarConf = (HashMap) sessionScope.get("debugToolbar");

			if (toolbarConf != null) {

				return toolbarConf;

			} else {

				//construct new toolbar config				
				HashMap<String, Object> config = new HashMap();

				config.put("contentType", null);
				config.put("hidden", false);
				config.put("loaded", isLoaded());
				config.put("logMessages", new ArrayList<JObject>());
				config.put("timers", new java.util.HashMap());
				config.put("messagesDetached", false);
				config.put("hideLogTypes", new ArrayList<String>());
				config.put("showLists", true);
				config.put("consolePath", FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
						+ "/debugToolbarConsole.xsp");

				setToolbarConfig(config);

				return config;
			}

		}

	}

	public void setToolbarConfig(HashMap config) {
		this.toolbarConfig = config;
		sessionScope.put("debugToolbar", config);
	}

}
