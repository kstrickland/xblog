package com.xblog.datamaintainer;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

import com.xblog.utils.JSFUtil;

public class LayoutMaintainer {

	public LayoutMaintainer() {
	}

	/**
	 * Mark the currently published layout as NOT published
	 * 
	 * @param newLayoutUnid
	 *            String - the UNID of the NEW published layout
	 */
	public static void unPublishCurrentLayout(String newLayoutUnid) {
		try {
			Database db = JSFUtil.getCurrentDatabase();
			View layoutView = db.getView("layouts");
			ViewEntryCollection entCol = layoutView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			while (ent != null) {
				if (ent.getDocument().getItemValueString("publish").equalsIgnoreCase("yes")
						&& !newLayoutUnid.equals(ent.getDocument().getUniversalID())) {
					Document doc = ent.getDocument();
					doc.replaceItemValue("publish", "No");
					doc.save();
				}
				ent = entCol.getNextEntry(ent);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the currently published layout
	 * 
	 * @return
	 */
	public static String getCurrentLayoutUnid() {
		try {
			Database db = JSFUtil.getCurrentDatabase();
			View layoutView = db.getView("layouts");
			ViewEntryCollection entCol = layoutView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			while (ent != null) {
				if (ent.getDocument().getItemValueString("publish").equalsIgnoreCase("yes")) {
					Document doc = ent.getDocument();
					return doc.getUniversalID();
				}
				ent = entCol.getNextEntry(ent);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param layoutName String - The name of the layout to publish
	 */
	public static void setLayoutPublished(String layoutName) {
		try {
			Database db = JSFUtil.getCurrentDatabase();
			View layoutView = db.getView("layouts");
			ViewEntryCollection entCol = layoutView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			while (ent != null) {
				if (ent.getDocument().getItemValueString("layoutName").equalsIgnoreCase(layoutName)) {
					Document doc = ent.getDocument();
					doc.replaceItemValue("publish", "Yes");
					doc.save();
				}
				ent = entCol.getNextEntry(ent);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	public static String getLayoutUnid(String layoutName) {
		try {
			Database db = JSFUtil.getCurrentDatabase();
			View layoutView = db.getView("layouts");
			ViewEntryCollection entCol = layoutView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			while (ent != null) {
				if (ent.getDocument().getItemValueString("layoutName").equalsIgnoreCase(layoutName)) {
					return ent.getDocument().getUniversalID();
				}
				ent = entCol.getNextEntry(ent);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}
}
