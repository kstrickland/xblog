package com.xblog.frontend;

/**
 * This class is a wrapper for Akismet which protects from spam postings via 
 * comments. To learn more about Akismet please visit:
 * https://akismet.com/
 */

import java.util.Map;

import net.sf.akismet.Akismet;

import com.xblog.utils.XBlogUtils;

public class CommentSpamCheck {

	private Akismet akismet;
	private String apiKey = (String) XBlogUtils.getCurrentInstance().getConfigValue("AkismetAPIKey", 0);
	private String blog = (String) XBlogUtils.getCurrentInstance().getConfigValue("BlogURL", 0);

	/**
	 * Create a new instance of Akismet
	 */
	public CommentSpamCheck() {
		try {
			akismet = new Akismet(apiKey, blog);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		setProxyConfig();
	}

	/**
	 * determine if this comment is spam
	 * 
	 * @param ipAddress
	 * @param userAgent
	 * @param referrer
	 * @param permalink
	 * @param commentType
	 * @param author
	 * @param authorEmail
	 * @param authorUrl
	 * @param commentContent
	 * @param other
	 * @return boolean
	 */
	public boolean isCommentSpam(String ipAddress, String userAgent, String referrer, String permalink, String commentType, String author,
			String authorEmail, String authorUrl, String commentContent, Map other) {
		if (isAPIValid()) {
			return akismet.commentCheck(ipAddress, userAgent, referrer, permalink, commentType, author, authorEmail, authorUrl,
					commentContent, other);
		}
		return false;
	}

	/**
	 * Check if an API Key is present and if so, check if it's valid
	 * 
	 * @return boolean - true if valid
	 */
	public boolean isAPIValid() {
		boolean result = false;
		if (apiKey != null) {
			if (apiKey.isEmpty()) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * Setup any proxy configuration that may be needed
	 */
	public void setProxyConfig() {
		String useProxy = (String) XBlogUtils.getCurrentInstance().getConfigValue("UseProxy", 0);

		if (useProxy.equalsIgnoreCase("yes")) {
			String proxyHost = (String) XBlogUtils.getCurrentInstance().getConfigValue("ProxyHostName", 0);
			int proxyPort = Integer.parseInt(XBlogUtils.getCurrentInstance().getConfigValue("ProxyPortNumber", 0).toString());
			if (proxyPort == 0) {
				proxyPort = 80;
			}
			if (proxyHost != null) {
				if (!proxyHost.isEmpty()) {
					String proxyUser = (String) XBlogUtils.getCurrentInstance().getConfigValue("ProxyUserName", 0);
					String proxyPassword = (String) XBlogUtils.getCurrentInstance().getConfigValue("ProxyPassword", 0);
					if (proxyUser != null && proxyPassword != null) {
						akismet.setProxyAuthenticationConfiguration(proxyUser, proxyPassword);
					}
					akismet.setProxyConfiguration(proxyHost, proxyPort);
				}
			}
		}
	}

	/**
	 * Report a comment that should have been marked as spam
	 * 
	 * @param ipAddress
	 * @param userAgent
	 * @param referrer
	 * @param permalink
	 * @param commentType
	 * @param author
	 * @param authorEmail
	 * @param authorUrl
	 * @param commentContent
	 * @param other
	 */
	public void submitSpam(String ipAddress, String userAgent, String referrer, String permalink, String commentType, String author,
			String authorEmail, String authorUrl, String commentContent, Map other) {
		akismet.submitSpam(ipAddress, userAgent, referrer, permalink, commentType, author, authorEmail, authorUrl, commentContent, other);
	}

	/**
	 * Report a comment that should not have been marked as spam
	 * 
	 * @param ipAddress
	 * @param userAgent
	 * @param referrer
	 * @param permalink
	 * @param commentType
	 * @param author
	 * @param authorEmail
	 * @param authorUrl
	 * @param commentContent
	 * @param other
	 */
	public void submitHam(String ipAddress, String userAgent, String referrer, String permalink, String commentType, String author,
			String authorEmail, String authorUrl, String commentContent, Map other) {
		akismet.submitHam(ipAddress, userAgent, referrer, permalink, commentType, author, authorEmail, authorUrl, commentContent, other);
	}

}
