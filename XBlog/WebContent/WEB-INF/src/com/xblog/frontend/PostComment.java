package com.xblog.frontend;

/**
 * This class handles all the data transfer, spam checking and comment content safety checking
 * of a posted comment and marks the comment as "Not Approved" if a spam check fails or
 * "Blocked" (if so configured) if it didn't pass Anti-Samy
 */

import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntryCollection;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogUtils;

public class PostComment {

	private Map<String, String> commentFailReason;
	private String authorName;
	private String authorEmailAddr;
	private String authorWebsite;
	private String commentText;
	private Document commentDoc;
	private String parentUNID;

	/**
	 * This is the constructor for the PostComment class. It initiates the safety check and spam check and starts the save process using the
	 * DataProvider bean. The trigger for this method is via the Frontend RPC
	 * @param nameStr - Author name
	 * @param emailStr - Author email
	 * @param websiteStr - Author website
	 * @param commentStr - The comment
	 * @param subjectStr - The subject
	 * @param parentUNID - The UNID of the post this comment is for
	 */
	public PostComment(String nameStr, String emailStr, String websiteStr, String commentStr, String subjectStr, String notifyMe,
			String parentUNID) {
		try {
			/*
			 * Would rather do this with our Data Provider as there are a lot more tools to manipulate
			 * data of the parent and common values for many of the fields. But this was causing major 
			 * issues with how a comment is saved from the frontend. So, we're doing it here instead
			 */
			this.parentUNID = parentUNID;
			Database thisDb = JSFUtil.getCurrentDatabase();
			commentDoc = thisDb.createDocument();
			commentDoc.replaceItemValue("form", "Comment");
			commentDoc.replaceItemValue("Name", nameStr);
			commentDoc.replaceItemValue("Email", emailStr);
			commentDoc.replaceItemValue("Subject", subjectStr);

			authorName = nameStr;
			authorEmailAddr = emailStr;
			if (websiteStr != null && websiteStr.indexOf("http://") == -1) {
				websiteStr = "http://" + websiteStr;
			} else if (websiteStr == null) {
				websiteStr = "";
			}
			commentDoc.replaceItemValue("Website", websiteStr);
			authorWebsite = websiteStr;
			DateTime nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(new Date());
			commentDoc.replaceItemValue("CreatedDate", nDate);

			if (notifyMe.equalsIgnoreCase("Yes")) {
				commentDoc.replaceItemValue("NotifyMe", "Yes");
			} else {
				commentDoc.replaceItemValue("NotifyMe", "No");
			}

			/*
			 * Check with AntiSamy to ensure Comment is safe
			 */
			/*Object useAntiSamy = XBlogUtils.getCurrentInstance().getConfigValue("UseAntiSamy", 0);
			if (useAntiSamy != null && useAntiSamy.toString().equalsIgnoreCase("yes")) {
				try {
					if (!isCommentSafe(commentStr)) {
						commentDoc.replaceItemValue("Blocked", "Yes");
					} else {
						commentDoc.replaceItemValue("Blocked", "No");
					}
				} catch (Exception e) {
					commentDoc.replaceItemValue("Blocked", "Yes");
				}
			}*/

			//We set this here because we may get back "cleaned" html
			// from anti-samy
			if (commentText == null) {
				commentText = commentStr;
			}
			Object useRichText = XBlogUtils.getCurrentInstance().getConfigValue("UseRichText", 0);
			if (useRichText != null && !useRichText.toString().equalsIgnoreCase("yes")) {
				commentText = commentText.replace("\n\r", "<br />");
				commentText = commentText.replace("\n", "<br />");
				commentText = replaceCommentLink(commentText);
			} else if (useRichText == null) {
				commentText = commentText.replace("\n\r", "<br />");
				commentText = commentText.replace("\n", "<br />");
				commentText = replaceCommentLink(commentText);
			}
			commentDoc.replaceItemValue("Comment", commentText);

			//Check for comment spam using Akismet
			Object useAkismet = XBlogUtils.getCurrentInstance().getConfigValue("UseAkismet", 0);
			if (useAkismet != null && useAkismet.toString().equalsIgnoreCase("yes")) {
				boolean commentSpam = isCommentSpam();
				if (commentSpam) {
					commentDoc.replaceItemValue("PendingApproval", "Yes");
				} else {
					commentDoc.replaceItemValue("PendingApproval", "No");
				}
			}
			commentDoc.replaceItemValue("Blocked", "No");
			updateCommentValues(commentDoc);
			sendNotification();
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This is the constructor for the PostComment class. It initiates the safety check and spam check and starts the save process using the
	 * DataProvider bean. The trigger for this method is via the Frontend RPC
	 * @param nameStr - Author name
	 * @param emailStr - Author email
	 * @param websiteStr - Author website
	 * @param commentStr - The comment
	 * @param subjectStr - The subject
	 * @param parentUNID - The UNID of the post this comment is for
	 */
	public PostComment(String nameStr, String emailStr, String websiteStr, String commentStr, String subjectStr, String notifyMe,
			String parentUNID, Boolean useAntiSamy) {
		try {
			/*
			 * Would rather do this with our Data Provider as there are a lot more tools to manipulate
			 * data of the parent and common values for many of the fields. But this was causing major 
			 * issues with how a comment is saved from the frontend. So, we're doing it here instead
			 */
			this.parentUNID = parentUNID;
			Database thisDb = JSFUtil.getCurrentDatabase();
			commentDoc = thisDb.createDocument();
			commentDoc.replaceItemValue("form", "Comment");
			commentDoc.replaceItemValue("Name", nameStr);
			commentDoc.replaceItemValue("Email", emailStr);
			commentDoc.replaceItemValue("Subject", subjectStr);

			authorName = nameStr;
			authorEmailAddr = emailStr;
			if (websiteStr != null && websiteStr.indexOf("http://") == -1) {
				websiteStr = "http://" + websiteStr;
			} else if (websiteStr == null) {
				websiteStr = "";
			}
			commentDoc.replaceItemValue("Website", websiteStr);
			authorWebsite = websiteStr;
			DateTime nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(new Date());
			commentDoc.replaceItemValue("CreatedDate", nDate);

			if (notifyMe.equalsIgnoreCase("Yes")) {
				commentDoc.replaceItemValue("NotifyMe", "Yes");
			} else {
				commentDoc.replaceItemValue("NotifyMe", "No");
			}

			/*
			 * Check with AntiSamy to ensure Comment is safe
			 */
			/*if (useAntiSamy) {
				try {
					if (!isCommentSafe(commentStr)) {
						commentDoc.replaceItemValue("Blocked", "Yes");
					} else {
						commentDoc.replaceItemValue("Blocked", "No");
					}
				} catch (Exception e) {
					commentDoc.replaceItemValue("Blocked", "Yes");
				}
			} else {
				commentDoc.replaceItemValue("Blocked", "Yes");
				commentDoc.replaceItemValue("BlockedReason", "AntiSamy is enabled but AntiSamy scanning failed");
			}*/

			//We set this here because we may get back "cleaned" html
			// from anti-samy
			if (commentText == null) {
				commentText = commentStr;
			}
			Object useRichText = XBlogUtils.getCurrentInstance().getConfigValue("UseRichText", 0);
			if (useRichText != null && !useRichText.toString().equalsIgnoreCase("yes")) {
				commentText = commentText.replace("\n\r", "<br />");
				commentText = commentText.replace("\n", "<br />");
				commentText = replaceCommentLink(commentText);
			} else if (useRichText == null) {
				commentText = commentText.replace("\n\r", "<br />");
				commentText = commentText.replace("\n", "<br />");
				commentText = replaceCommentLink(commentText);
			}
			//System.out.println("PostComment.commentText = " + commentText);
			commentDoc.replaceItemValue("Comment", commentText);

			//Check for comment spam using Akismet
			boolean commentSpam = isCommentSpam();
			if (commentSpam) {
				commentDoc.replaceItemValue("PendingApproval", "Yes");
			} else {
				commentDoc.replaceItemValue("PendingApproval", "No");
			}
			commentDoc.replaceItemValue("Blocked", "No");
			updateCommentValues(commentDoc);
			sendNotification();
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method checks if the comment is potentially spam using Akismet and the CommentSpamCheck class
	 * @return boolean - true if spam
	 */
	private boolean isCommentSpam() {
		try {
			CommentSpamCheck commentSpam = new CommentSpamCheck();
			ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletRequest request = (HttpServletRequest) extContext.getRequest();
			String ipAddress = request.getRemoteAddr().toString();
			String userAgent = request.getHeader("User-Agent");
			String referrer = request.getHeader("Referer");
			String permalink = XBlogUtils.getCurrentInstance().getCurrentUrl() + "?documentId=" + request.getParameter("documentId");
			commentDoc.replaceItemValue("UserAgent", userAgent);
			commentDoc.replaceItemValue("Referrer", referrer);
			commentDoc.replaceItemValue("CommentType", "comment");
			commentDoc.replaceItemValue("IPAddress", ipAddress);
			Map other = extContext.getRequestHeaderValuesMap();
			return commentSpam.isCommentSpam(ipAddress, userAgent, referrer, permalink, "comment", authorName, authorEmailAddr,
					authorWebsite, commentText, other);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Send a notification email to the owner that a new comment has been posted
	 */
	@SuppressWarnings("unchecked")
	private void sendNotification() {
		try {
			String commentNotify = (String) XBlogUtils.getCurrentInstance().getConfigValue("CommentEmailNotify", 0);
			if (commentNotify != null && commentNotify.equalsIgnoreCase("Yes")) {
				Document emailDoc = NotesContext.getCurrent().getCurrentDatabase().createDocument();
				emailDoc.replaceItemValue("form", "memo");
				String msg = "A new comment has been received at " + XBlogUtils.getCurrentInstance().getConfigValue("BlogName", 0)
						+ " you can read the comment at " + XBlogUtils.getCurrentInstance().getBlogUrl() + "/default.xsp?documentId="
						+ JSFUtil.getParamValue("documentId");
				String ownerEmail = XBlogUtils.getCurrentInstance().getConfigValue("ContactEmail", 0).toString();
				Map viewScope = (Map) JSFUtil.getVariableValue("viewScope");
				String subject = "Comment posted for '" + viewScope.get("pageTitle") + "'";
				emailDoc.replaceItemValue("Subject", subject);
				emailDoc.replaceItemValue("Body", msg);
				emailDoc.send(ownerEmail);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send notifications to users that a new comment has been posted
	 */
	@SuppressWarnings("unchecked")
	private void sendUserNotification() {
		try {
			String parentUnid = JSFUtil.getParamValue("documentId");
			View commentView = NotesContext.getCurrent().getCurrentDatabase().getView("(luComments)");
			DocumentCollection docCol = commentView.getAllDocumentsByKey(parentUnid);
			Document doc = docCol.getFirstDocument();
			while (doc != null) {
				if (doc.getItemValueString("NotifyMe").equalsIgnoreCase("Yes")) {
					Document emailDoc = NotesContext.getCurrent().getCurrentDatabase().createDocument();
					Map viewScope = (Map) JSFUtil.getVariableValue("viewScope");
					String subject = "Comment posted for '" + viewScope.get("pageTitle") + "'";
					String msg = "A new comment has been received at " + XBlogUtils.getCurrentInstance().getConfigValue("BlogName", 0)
							+ " you can read the comment <a href=\"" + XBlogUtils.getCurrentInstance().getBlogUrl()
							+ "/default.xsp?documentId=" + JSFUtil.getParamValue("documentId") + "\">here</a>";
					String userEmail = doc.getItemValueString("Email");
					emailDoc.replaceItemValue("subject", subject);
					emailDoc.replaceItemValue("body", msg);
					emailDoc.replaceItemValue("Form", "memo");
					emailDoc.send(userEmail);
				}
				doc = docCol.getNextDocument(doc);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Replace the "http://www.someplace.com" or www.someplace.com with an html link that says {link}
	 * @param String - commentTxt - the text of the comment to work on
	 * @return String - the updated html
	 */
	private String replaceCommentLink(String commentTxt) {
		String regex = "\\(?\\b(http://|https://|ftp://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(commentText);
		while (m.find()) {
			String urlStrOrig = m.group();
			String urlStr;
			if (!urlStrOrig.startsWith("http://") && !urlStrOrig.startsWith("ftp://") && !urlStrOrig.startsWith("https://")) {
				urlStr = "http://" + urlStrOrig;
			} else {
				urlStr = urlStrOrig;
			}
			commentTxt = commentTxt.replace(urlStrOrig, "<a href='" + urlStr + "' target='_blank'>{link}</a>");
		}
		return commentTxt;
	}

	/**
	 * Update values specific to the Comment form
	 * 
	 * @param doc
	 * @throws NotesException
	 */
	private void updateCommentValues(Document doc) throws NotesException {
		Map sessionScope = (Map) JSFUtil.getVariableValue("sessionScope");
		String parentUnid = this.parentUNID;
		if (parentUnid == null) {
			if (sessionScope.containsKey("parentUNID")) {
				parentUnid = sessionScope.get("parentUNID").toString();
			} else {
				Map paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
				String paramDocId = null;
				if (paramMap.containsKey("documentId")) {
					paramDocId = paramMap.get("documentId").toString();
				} else {
					paramDocId = this.parentUNID;
				}
				if (paramDocId != null && paramDocId.equalsIgnoreCase(commentDoc.getUniversalID())) {
					parentUnid = commentDoc.getParentDocumentUNID();
				} else {
					parentUnid = paramDocId;
				}
			}
		}
		Document parentDoc = NotesContext.getCurrent().getCurrentDatabase().getDocumentByUNID(parentUnid);
		if (parentDoc != null) {
			commentDoc.replaceItemValue("ParentUNID", parentUnid);
			int commentNumber = getCommentNumber();
			commentDoc.replaceItemValue("CommentNumber", commentNumber);
			//parentDoc.replaceItemValue("numberComments", commentNumber);
			String permalink = XBlogUtils.getCurrentInstance().getRelativeUrl() + "/default.xsp?documentId=" + parentUnid + "#"
					+ commentNumber;
			commentDoc.replaceItemValue("Permalink", permalink);
			commentDoc.makeResponse(parentDoc);
			//parentDoc.save(true, false);
			commentDoc.save(true, true);
		}
		if (sessionScope.containsKey("parentUNID")) {
			sessionScope.remove("parentUNID");
		}
	}

	/**
	 * Get the number of comments for a post
	 * 
	 * @return
	 * @throws NotesException
	 */
	private int getCommentNumber() throws NotesException {
		View commentView = NotesContext.getCurrent().getCurrentDatabase().getView("(luComments)");
		ViewEntryCollection entCol = commentView.getAllEntriesByKey(parentUNID);
		if (entCol.getCount() > 0) {
			return entCol.getCount() + 1;
		} else {
			return 1;
		}
	}
}
