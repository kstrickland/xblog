package com.xblog.renderkit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.components.SideBarItem;
import com.xblog.frontend.DateFormatter;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogAppScope;
import com.xblog.utils.XBlogResources;
import com.xblog.utils.XBlogUtils;

/**
 * The renderer for a Sidebar item. This builds the HTML for an individual
 * sidebar item
 * @author Keith Strickland
 *
 */
public class SideBarItemRenderer extends Renderer {

	@Override
	public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
		super.encodeBegin(context, component);
		SideBarItem c = (SideBarItem) component;
		String type = c.getSideBarType();
		ResponseWriter writer = context.getResponseWriter();
		Map appScope = (Map) JSFUtil.getVariableValue("applicationScope");
		Map sideBarMap;
		//Check if we've already got a sideBarMap applicationScope variable
		if (appScope.containsKey("sideBarMap")) {
			sideBarMap = (Map) appScope.get("sideBarMap");
		} else {
			sideBarMap = new HashMap<String, String>();
		}

		//Ensure the necessary resources are available to the page
		XBlogResources.addEncodeResource(context, XBlogResources.dojoxPortlet);
		XBlogResources.addEncodeResource(context, XBlogResources.dojoxFeedPortlet);
		XBlogResources.addEncodeResource(context, XBlogResources.dojoxPortletCss);

		//Check if this sidebar item is in the sidebar map
		StringBuilder sb = new StringBuilder();
		if (sideBarMap.containsKey(c.getSideBarDocUNID())) {
			sb.append(sideBarMap.get(c.getSideBarDocUNID()).toString());
		} else {
			sb.append(buildHtml(component));
			if (!type.equals("Most Popular") && !type.equals("Most Commented") && !type.equals("Last Comments Posted")
					&& !type.equals("Now Viewing")) {
				sideBarMap.put(c.getSideBarDocUNID(), sb.toString());
				appScope.put("sideBarMap", sideBarMap);
			}
		}
		writer.append(sb.toString());
	}

	@Override
	public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
		super.encodeChildren(context, component);
	}

	@Override
	public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
		super.encodeEnd(context, component);
	}

	/**
	 * Build the html for the outter wrapper for all html sidebar items
	 * @param - javax.faces.component.UIComponent - The sidebar item component
	 * @return - java.lang.String - The html for the sidebar item
	 */
	private String buildHtml(UIComponent component) {
		SideBarItem c = (SideBarItem) component;
		String title = c.getSideBarTitle();
		String type = c.getSideBarType();
		StringBuilder sb = new StringBuilder();
		String dijitId = "portlet_" + c.getSideBarDocUNID();
		if (type != null && !type.equalsIgnoreCase("Tag Cloud") && !type.equalsIgnoreCase("subscribe")) {
			if (type.equalsIgnoreCase("RSS Feed")) {
				sb.append("<div id='" + dijitId + "' dojoType='dojox.widget.ExpandableFeedPortlet' title='" + title + "'");
				sb.append("url='" + c.getFeedPortletUrl() + "' + maxResults='" + c.getSideBarNumberItems() + "'>");
			} else {
				sb.append("<div id='" + dijitId + "'" + " dojoType='dojox.widget.Portlet' title='" + title + "'>");
			}
			sb.append("<div class='SideBarContent'>");
			if (type.equalsIgnoreCase("archives")) {
				sb.append(buildArchive(c));
			} else if (type.equalsIgnoreCase("categories")) {
				sb.append(buildCategoryHtml());
			} else if (type.equalsIgnoreCase("html")) {
				sb.append(buildHtmlContent(component));
			} else if (type.equalsIgnoreCase("contact")) {
				sb.append(buildBio());
			} else if (type.equalsIgnoreCase("Blog Roll")) {
				sb.append(buildBlogRoll());
			} else if (type.equalsIgnoreCase("Most Popular")) {
				sb.append(buildMost(c, "Popular"));
			} else if (type.equalsIgnoreCase("Most Commented")) {
				sb.append(buildMost(c, "Comment"));
			} else if (type.equalsIgnoreCase("Downloads")) {
				sb.append(buildDownloadsHtml(c));
			} else if (type.equalsIgnoreCase("Pages")) {
				sb.append(buildPages(c));
			} else if (type.equalsIgnoreCase("Last Comments Posted")) {
				sb.append(buildLastComments(c));
			} else if (type.equalsIgnoreCase("My Twitter")) {
				sb.append(buildTwitter(c));
			} else if (type.equalsIgnoreCase("Now Viewing")) {
				sb.append(buildNowViewing(c));
			}
			sb.append("</div></div>");

			//Cache the sidebar if it can be cached
			Map appScope = (Map) JSFUtil.getVariableValue("applicationScope");
			Map<String, String> sideBarMap;
			if (appScope.containsKey("sideBarMap")) {
				sideBarMap = (Map) appScope.get("sideBarMap");
			} else {
				sideBarMap = new HashMap<String, String>();
			}
		}
		return sb.toString();
	}

	/**
	 * Build the html list for categories. This is actually deprecated but we'll leave it for now
	 * @return - java.lang.String - The html list of categories
	 */
	private String buildCategoryHtml() {
		// TODO: Determine the URL for categories
		try {
			View categoryView = JSFUtil.getCurrentDatabase().getView("(luCategories)");
			Vector vals = categoryView.getColumnValues(0);
			StringBuilder sb = new StringBuilder();
			if (vals != null) {
				sb.append("<ul class='CategoryList'>");
				for (Object val : vals) {
					sb.append("<li class='CategoryListItem'>" + val.toString() + "</li>");
				}
				sb.append("</ul>");
				return sb.toString();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the Archive sidebar item
	 * @param c - com.xblog.components.SideBarItem - The sidebar item we're building the html for
	 * @return java.lang.String - The HTML for the archive sidebar block
	 */
	private String buildArchive(SideBarItem c) {
		try {
			View archiveView = JSFUtil.getCurrentDatabase().getView("(luSidebarArchives)");
			ViewEntryCollection entCol = archiveView.getAllEntries();
			StringBuilder sb = new StringBuilder();
			if (entCol.getCount() > 0) {
				sb.append("<ul class='SidebarList ArchiveList'>");
				ViewEntry ent = entCol.getFirstEntry();
				List<String> inArchive = new ArrayList();
				String indexStr = null;
				int count = 1;
				XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
				while (ent != null && count <= c.getSideBarNumberItems()) {
					indexStr = ent.getColumnValues().get(1).toString() + "-" + ent.getColumnValues().get(2).toString();
					if (!inArchive.contains(indexStr)) {
						String createdDateStr = ent.getColumnValues().get(0).toString();
						Date archiveDate = NotesContext.getCurrent().getCurrentSession().createDateTime(createdDateStr).toJavaDate();
						String dateDisplay = DateFormatter.getFormattedDate(c.getSideBarArchiveDateFormat(), archiveDate);
						sb.append("<li class='SidebarListItem ArchiveItem'>");
						sb.append("<a href='" + xbUtils.getRelativeUrl() + "/default.xsp?archive=" + indexStr + "'>" + dateDisplay + "</a>");
						sb.append("</li>");
						count++;
						inArchive.add(indexStr);
					}
					ent = entCol.getNextEntry(ent);
				}
				sb.append("</ul><br>");
				sb.append("<span class='AllItems'>");
				sb.append("<a href='" + xbUtils.getRelativeUrl() + "/default.xsp?fullarchive=yes' title='Full Archive'>Full Archive</a>");
				sb.append("</span>");
			}
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the contact sidebar item
	 * @return - java.lang.String - The html for the contact side bar item
	 */
	private String buildBio() {
		try {
			StringBuilder sb = new StringBuilder();			
			sb.append("<ul class='SidebarList ContactList'>");
			XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
			if (xbUtils.getConfigValue("ContactName", 0) != null) {
				String emailLink = "";
				if (xbUtils.getConfigValue("ContactEmail", 0) != null && !xbUtils.getConfigValue("ContactEmail", 0).toString().isEmpty()) {
					emailLink = "Email: <a class='EmailLink' href='mailto:" + xbUtils.getConfigValue("ContactEmail", 0).toString()
							+ "' target='_blank'>" + xbUtils.getConfigValue("ContactName", 0).toString() + "</a>";
				} else {
					emailLink = xbUtils.getConfigValue("ContactName", 0).toString();
				}
				sb.append("<li class='SidebarListItem ContactListItem'>" + emailLink + "</li>");
				if (xbUtils.getConfigValue("ContactFacebook", 0) != null
						&& !xbUtils.getConfigValue("ContactFacebook", 0).toString().isEmpty()) {
					emailLink = "Facebook: <a class='FacebookLink' href='" + xbUtils.getConfigValue("ContactFacebook", 0)
							+ "' target='_blank'>" + xbUtils.getConfigValue("ContactName", 0).toString() + "</a>";
					sb.append("<li class='SidebarListItem ContactListItem'>" + emailLink + "</li>");
				}
				if (xbUtils.getConfigValue("ContactBleedYellow", 0) != null
						&& !xbUtils.getConfigValue("ContactBleedYellow", 0).toString().isEmpty()) {
					emailLink = "Bleed Yellow: <a class='BleedYellowLink' href='" + xbUtils.getConfigValue("ContactBleedYellow", 0)
							+ "' target='_blank'>" + xbUtils.getConfigValue("ContactName", 0).toString() + "</a>";
					sb.append("<li class='SidebarListItem ContactListItem'>" + emailLink + "</li>");
				}
				if (xbUtils.getConfigValue("ContactLDD", 0) != null && !xbUtils.getConfigValue("ContactLDD", 0).toString().isEmpty()) {
					emailLink = "LDD: <a class='LDDLink' href='" + xbUtils.getConfigValue("ContactLDD", 0) + "' target='_blank'>"
							+ xbUtils.getConfigValue("ContactName", 0).toString() + "</a>";
					sb.append("<li class='SidebarListItem ContactListItem'>" + emailLink + "</li>");
				}
			}
			if (xbUtils.getConfigValue("ContactMSN", 0) != null && !xbUtils.getConfigValue("ContactMSN", 0).toString().isEmpty()) {
				sb.append("<li class='SidebarListItem ContactListItem'>MSN: " + xbUtils.getConfigValue("ContactMSN", 0).toString() + "</li>");
			}
			if (xbUtils.getConfigValue("ContactAIM", 0) != null && !xbUtils.getConfigValue("ContactAIM", 0).toString().isEmpty()) {
				sb.append("<li class='SidebarListItem ContactListItem'>AIM: " + xbUtils.getConfigValue("ContactAIM", 0).toString() + "</li>");
			}
			if (xbUtils.getConfigValue("ContactGoogleTalk", 0) != null
					&& !xbUtils.getConfigValue("ContactGoogleTalk", 0).toString().isEmpty()) {
				sb.append("<li class='SidebarListItem ContactListItem'>Google Talk: ");
				sb.append(xbUtils.getConfigValue("ContactGoogleTalk", 0).toString() + "</li>");
			}
			if (xbUtils.getConfigValue("ContactYahoo", 0) != null && !xbUtils.getConfigValue("ContactYahoo", 0).toString().isEmpty()) {
				sb.append("<li class='SidebarListItem ContactListItem'>Yahoo!: " + xbUtils.getConfigValue("ContactYahoo", 0).toString()	+ "</li>");
			}
			sb.append("</ul>");
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Builds both the "Most Popular" and "Most Commented" lists
	 * @param - com.xblog.components.SideBarItem - The sidebar item
	 * @param mostType - java.lang.String - The type of sidebar item. Allowed values "Popular" or "Comment"
	 * @return - java.lang.String - The html for the "Most Popular" or "Most Commented" sidebar items
	 */
	private String buildMost(SideBarItem c, String mostType) {
		try {
			String html = "";
			View luView = null;
			String ListStyleClass = "";
			String ListItemClass = "";
			if (mostType.equalsIgnoreCase("Popular")) {
				luView = JSFUtil.getCurrentDatabase().getView("(luPopularNum)");
				ListStyleClass = "SidebarList MostPopularList";
				ListItemClass = "SidebarListItem PopularListItem";
			} else if (mostType.equalsIgnoreCase("Comment")) {
				luView = JSFUtil.getCurrentDatabase().getView("(luCommentNum)");
				ListStyleClass = "SidebarList MostCommentedList";
				ListItemClass = "SidebarListItem PopularListItem";
			}
			ViewEntryCollection entCol = luView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			int count = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("<ul class='" + ListStyleClass + "'>");
			XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
			while (ent != null && count <= c.getSideBarNumberItems()) {
				sb.append("<li class='" + ListItemClass + "'>");
				sb.append("<a href='" + xbUtils.getRelativeUrl() + "/default.xsp?documentId=" + ent.getColumnValues().get(2).toString() + "' ");
				sb.append("title='" + ent.getColumnValues().get(1).toString() + "'>" + ent.getColumnValues().get(1).toString() + "</a>");
				sb.append("</li>");
				count++;
				ent = entCol.getNextEntry(ent);
			}
			sb.append("</ul>");
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the "Blog Roll"/"Links" sidebar item
	 * @return - java.lang.String - The html for the "Blog Roll"/"Links" sidebar item
	 */
	private String buildBlogRoll() {
		try {
			View blogRollView = JSFUtil.getCurrentDatabase().getView("(luLinks)");
			ViewEntryCollection entCol = blogRollView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			StringBuilder sb = new StringBuilder();
			sb.append("<ul class='SidebarList LinksList'>");
			XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
			while (ent != null) {
				sb.append("<li class='SidebarListItem LinkListItem'>");
				if (!ent.getColumnValues().get(4).toString().isEmpty()) {
					sb.append("<a href='" + ent.getColumnValues().get(4).toString() + "'>");
					sb.append("<img src='" + xbUtils.getRelativeUrl() + "/RSS.png'/></a> : ");
				}
				sb.append("<a href='" + ent.getColumnValues().get(2) + "'");
				if (ent.getColumnValues().get(3).toString().equalsIgnoreCase("yes")) {
					sb.append(" target='_blank'");
				}
				sb.append(">" + ent.getColumnValues().get(1).toString() + "</a></li>");
				ent = entCol.getNextEntry(ent);
			}
			sb.append("</ul>");
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the "HTML" sidebar item
	 * @param component - javax.faces.components.UIComponent - the sidebar item
	 * @return - java.lang.String - The html for the "HTML" sidebar item
	 */
	private String buildHtmlContent(UIComponent component) {
		SideBarItem c = (SideBarItem) component;
		return c.getSideBarContent();
	}

	/**
	 * Build the html for the downloads sidebar item
	 * @param c - com.xblog.components.SideBarItem - The sidebar item
	 * @return - java.lang.String - The html for the downloads sidebar item
	 */
	private String buildDownloadsHtml(SideBarItem c) {
		try {
			View downloadsView = JSFUtil.getCurrentDatabase().getView("(luDownloads)");
			ViewEntryCollection entCol = downloadsView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();
			StringBuilder sb = new StringBuilder();
			sb.append("<ul class='SidebarList DownloadsList'>");
			int count = 1;
			XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
			while (ent != null && count <= c.getSideBarNumberItems()) {
				sb.append("<li class='SidebarListItem DownloadListItem'>");
				sb.append("<a href='" + xbUtils.getRelativeUrl() + "/default.xsp?documentId=" + ent.getColumnValues().get(3).toString());
				sb.append("' title='" + ent.getColumnValues().get(2).toString() + "'>");
				sb.append(ent.getColumnValues().get(0).toString() + "</a>");
				sb.append("</li>");
				count++;
				ent = entCol.getNextEntry(ent);
			}
			sb.append("</ul><br>");
			sb.append("<span class='AllItems'>");
			sb.append("<a href='" + xbUtils.getRelativeUrl() + "/default.xsp?downloads=yes' title='All Downloads'>All Downloads</a>");
			sb.append("</span>");
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the Pages sidebar item
	 * @param c - com.xblog.components.SideBarItem - The sidebar item
	 * @return - java.lang.String - The HTML for the "Pages" sidebar item
	 */
	private String buildPages(SideBarItem c) {
		try {
			StringBuilder sb = new StringBuilder();
			View pagesView = JSFUtil.getCurrentDatabase().getView("(homePagesByDate)");
			ViewEntryCollection entCol = pagesView.getAllEntries();
			if (entCol.getCount() == 0) {
				sb.append("<b>No Pages Found</b>");
			} else {
				ViewEntry ent = entCol.getFirstEntry();
				sb.append("<ul class='SidebarList PagesList'>");
				int count = 1;
				XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
				while (ent != null && count <= c.getSideBarNumberItems()) {
					sb.append("<li class='SidebarListItem PagesListItem'>");
					sb.append("<a href='" + xbUtils.getRelativeUrl() + "/default.xsp?documentId=" + ent.getColumnValues().get(9).toString() + "&type=page'");
					sb.append("title='" + ent.getColumnValues().get(1).toString() + "'>" + ent.getColumnValues().get(1).toString() + "</a>");
					sb.append("</li>");
					count++;
					ent = entCol.getNextEntry(ent);
				}
				sb.append("</ul>");
			}
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the latest comments sidebar item
	 * @param c - com.xblog.components.SideBarItem - the sidebar item
	 * @return - java.lang.String - The html for the latest comments sidebar item
	 */
	private String buildLastComments(SideBarItem c) {
		try {
			StringBuilder sb = new StringBuilder();
			View commentsView = JSFUtil.getCurrentDatabase().getView("(luSidebarComments)");
			ViewEntryCollection entCol = commentsView.getAllEntries();
			if (entCol.getCount() == 0) {
				sb.append("<b>No Comments Found</b>");
			} else {
				ViewEntry ent = entCol.getFirstEntry();
				sb.append("<ul class='SidebarList CommentsList'>");
				int count = 1;
				while (ent != null && count <= c.getSideBarNumberItems()) {
					sb.append("<li class='SidebarListItem CommentListItem'>");
					sb.append("<a href='" + ent.getColumnValues().get(1) + "' ");
					sb.append("title='"	+ ent.getColumnValues().get(2) + "'>");
					sb.append(ent.getColumnValues().get(2) + ":" + ent.getColumnValues().get(3) + "</a>");
					sb.append("</li>");
					count++;
					ent = entCol.getNextEntry(ent);
				}
				sb.append("</ul>");
			}
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build the html for the twitter sidebar item
	 * @param c - com.xblog.components.SideBarItem - The sidebar item
	 * @return - Java.lang.String - The html for the twitter feed sidebar item
	 */
	private String buildTwitter(SideBarItem c) {
		// System.out.println("starting buildTwitter");
		StringBuilder sb = new StringBuilder();
		sb.append("<script src=\"http://widgets.twimg.com/j/2/widget.js\"></script>");
		sb.append("<script>new TWTR.Widget({" + "version: 2," + "type: 'profile'," + "rpp: 4," + "interval: 6000,");
		sb.append("width:" + c.getTwitterSidebarWidth() + "," + "height: 300,");
		sb.append("theme: {shell: {background: 'c.getTwitterBackgroundColor()',color: '" + c.getTwitterTweetColor() + "'},");
		sb.append("tweets: {background: '" + c.getTwitterBackgroundColor() + "',color: '" + c.getTwitterTweetColor() + "',");
		sb.append("links: '" + c.getTwitterLinkColor() + "'}},features: {scrollbar: " + c.isShowTwitterScrollbar() + ",");
		sb.append("loop: false,live: false,hashtags: true,timestamp: true,avatars: " + c.isShowTwitterAvatar() + ",");
		sb.append("behavior: 'all'}}).render().setUser('" + c.getTwitterUser() + "').start();</script>");
		return sb.toString();
	}

	/**
	 * Build the html for the "Now Viewing" sidebar item
	 * @param c - com.xblog.components.SideBarItem - The sidebar item
	 * @return - java.lang.String - The html for the "Now Viewing" sidebar item
	 */
	private String buildNowViewing(SideBarItem c) {
		StringBuilder sb = new StringBuilder();
		try {
			XBlogAppScope appScope = XBlogAppScope.getCurrentInstance();
			Map<String, Integer> viewing = appScope.getNowViewing();
			String relUrl = XBlogUtils.getCurrentInstance().getRelativeUrl();
			Database db = JSFUtil.getCurrentDatabase();
			if (viewing != null && !viewing.isEmpty()) {
				sb.append("<ul class='SidebarList NowViewingList'>");
				int count = 1;
				for (String unid : viewing.keySet()) {
					if (count <= c.getSideBarNumberItems()) {
						Document doc = db.getDocumentByUNID(unid);
						sb.append("<li class='SidebarListItem NowViewingListItem'>");
						sb.append("<a href='" + relUrl + "/default.xsp?documentId="	+ unid + "'");
						sb.append("title='" + doc.getItemValueString("title") + "'>" + doc.getItemValueString("Title") + "</a>");
						sb.append("(" + viewing.get(unid) + " people viewing)</li>");
					}
					count++;
				}
				sb.append("</ul>");
			} else {
				sb.append("<span class='NowViewingEmpty'>No Articles currently being read</span>");
			}
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

}
