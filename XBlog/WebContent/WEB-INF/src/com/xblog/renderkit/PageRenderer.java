package com.xblog.renderkit;

/**
 * This class is a direct extension of com.xblog.renderkit.PostRenderer. Since a page and a post
 * are virtually identical this is pretty much an empty class
 * @author Keith Strickland
 *
 */

public class PageRenderer extends PostRenderer {

}
