package com.xblog.renderkit;

import com.xblog.components.Post;
import com.xblog.utils.JSFUtil;

public class DownloadRenderer extends PostRenderer {

	private String buildAttachmentHtml(Post post) {
		StringBuilder sb = new StringBuilder();
		if (post.getAttachments() != null || JSFUtil.getParamValue("downloads") != null) {
			sb.append("<div class='AttachmentContainer'>");
			for (String attach : post.getAttachments()) {
				sb.append("<span class='BlogAttachmentLink'>");
				sb.append("<a href='" + attach + "'>" + attach.substring(attach.lastIndexOf('/') + 1) + "</a>");
				sb.append("</span>");
			}
			sb.append("</div>");
		}
		return sb.toString();
	}
}
