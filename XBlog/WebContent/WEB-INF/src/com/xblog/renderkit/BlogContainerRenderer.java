package com.xblog.renderkit;

/*
 * TODO: Need to actually parse the html for the blog container and then do whatever is necessary to include
 * whatever content is included and where it is included.
 */

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import com.ibm.xsp.renderkit.html_basic.PanelRenderer;
import com.xblog.renderkit.utils.RenderUtils;
import com.xblog.utils.DebugToolbar;
import com.xblog.utils.XBlogUtils;

public class BlogContainerRenderer extends PanelRenderer {

	@Override
	public void encodeBegin(FacesContext context,
			UIComponent component) throws IOException {
		super.encodeBegin(context, component);
		DebugToolbar debug = XBlogUtils.getCurrentInstance().dbar;
		String defaultHtml = RenderUtils.getDefaultHtml("layout", "ContentContainerLayout");
		debug.info("defaultHtml = " + defaultHtml.toString(), "BlogContainerRenderer.encodeBegin");
		ResponseWriter writer = context.getResponseWriter();
		String topHtml = getTopDefaultHtml(defaultHtml);
		debug.info("topHtml = " + topHtml, "BlogContainerRenderer.encodeBegin");
		writer.append(topHtml);
	}
	
	@Override
	public void encodeChildren(FacesContext context, UIComponent component)
			throws IOException {
		super.encodeChildren(context, component);
	}
	
	@Override
	public void encodeEnd(FacesContext context,
			UIComponent component) throws IOException {
		ResponseWriter writer = context.getResponseWriter();
		DebugToolbar debug = XBlogUtils.getCurrentInstance().dbar;
		String defaultHtml = RenderUtils.getDefaultHtml("layout", "ContentContainerLayout");
		String bottomHtml = getBottomDefaultHtml(defaultHtml);
		debug.info("bottomHtml = " + bottomHtml, "BlogContainerRenderer.encodeEnd");
		writer.append(bottomHtml);
		super.encodeEnd(context, component);
	}
	
	private String getTopDefaultHtml(String defaultHtml) {
		try {
			XBlogUtils.getCurrentInstance().dbar.info(
					"defaultHtml = " + defaultHtml,
					"BlogContainerRenderer.getTopDefaultHtml(String)");
			String topHtml = defaultHtml.substring(0, defaultHtml.indexOf("<$BlogContent$>"));
			topHtml = RenderUtils.buildGenericHtml(topHtml, null);
			return topHtml;
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private String getBottomDefaultHtml(String defaultHtml) {
		try {
			XBlogUtils.getCurrentInstance().dbar.info("defaultHtml = " + defaultHtml, "BlogContainerRenderer.getBottomDefaultHtml(String)");
			String bottomHtml = defaultHtml.substring(defaultHtml.indexOf("<$BlogContent$>") +15);
			bottomHtml = RenderUtils.buildGenericHtml(bottomHtml, null);
			return bottomHtml;
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
			return "";
		}
	}
}
