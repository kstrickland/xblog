package com.xblog.renderkit;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;

import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.components.Comment;
import com.xblog.renderkit.utils.RenderUtils;

/**
 * The renderer for com.xblog.components.Comment. This builds the HTML for an
 * individual comment
 * 
 * @author Keith Strickland
 */

public class CommentRenderer extends Renderer {

	private String commentHtml;
	private Map<String, Object> hotTags;

	@Override
	public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
		super.encodeBegin(context, component);
		Comment comment = (Comment) component;
		commentHtml = comment.getDefaultHtml();
		hotTags = getHotTagMap(comment);
		for (String replaceTag : hotTags.keySet()) {
			commentHtml = commentHtml.replace(replaceTag, hotTags.get(replaceTag).toString());
		}
		commentHtml = RenderUtils.replaceHotTags(commentHtml, "(luCustomHotTags)", 0, 1, comment);
		commentHtml = RenderUtils.replaceTranslations(commentHtml, comment);
		commentHtml = RenderUtils.cleanLeftoverTags(commentHtml);
	}

	@Override
	public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
		super.encodeChildren(context, component);
	}

	@Override
	public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
		super.encodeEnd(context, component);
		ResponseWriter writer = context.getResponseWriter();
		writer.append(commentHtml);
	}

	/**
	 * Build a hash map of hot tags and the tag's replacement html
	 * 
	 * @param post
	 *            - com.xblog.components.Comment - The comment
	 * @return - java.util.Map<String, Object> - The "<$HotTag$>" and
	 *         ValueToReplaceHotTagWith
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> getHotTagMap(Comment comment) {
		try {
			Map<String, Object> hotTagMap = new HashMap<String, Object>();
			Map<String, String> fieldMap = getFieldPropertyMap();
			Class klass = comment.getClass();
			View hotView = NotesContext.getCurrent().getCurrentDatabase().getView("hottags");
			ViewEntryCollection entCol = hotView.getAllEntriesByKey("Comment");
			ViewEntry ent = entCol.getFirstEntry();
			while (ent != null) {
				String field = ent.getColumnValues().get(2).toString().trim();
				String tag = ent.getColumnValues().get(1).toString().trim();
				String value = null;
				if (field.equalsIgnoreCase("author")) {
					value = RenderUtils.buildAuthorHtml(comment);
				} else if (field.equalsIgnoreCase("createdDate")) {
					value = RenderUtils.formatDate(comment);
				} else if (field.equalsIgnoreCase("Comment")) {
					value = RenderUtils.replaceEmoticons(comment.getCommentText(), comment);
				} else {
					Object tempValue = klass.getMethod(fieldMap.get(field)).invoke(comment);
					if (tempValue != null) {
						if (tempValue instanceof Integer) {
							value = ((Integer) tempValue).toString();
						} else {
							value = tempValue.toString();
						}
					} else {
						value = "";
					}
				}
				hotTagMap.put(tag, value);
				ViewEntry prevEnt = ent;
				ent = entCol.getNextEntry(ent);
				prevEnt.recycle();
			}
			return hotTagMap;
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Build a map of field names and their associated getter/setter in the
	 * com.xblog.components.Comment class
	 * 
	 * @return - java.util.Map<String, String> - a map of the fieldname and it's
	 *         associated method name
	 */
	private Map<String, String> getFieldPropertyMap() {
		Map<String, String> fieldMap = new HashMap<String, String>();
		fieldMap.put("Blocked", "isBlocked");
		fieldMap.put("CreatedDate", "getCreatedDate");
		fieldMap.put("CommentNumber", "getCommentNumber");
		fieldMap.put("Comment", "getCommentText");
		fieldMap.put("Department", "getDepartment");
		fieldMap.put("Email", "getEmailAddress");
		fieldMap.put("Gravatar", "getGravatar");
		fieldMap.put("IPAddress", "getIpAddress");
		fieldMap.put("Name", "getAuthor");
		fieldMap.put("NotifyUser", "isNotifyUser");
		fieldMap.put("ParentUNID", "getParentUnid");
		fieldMap.put("PendingApproval", "isPendingApproval");
		fieldMap.put("Referrer", "getReferrer");
		fieldMap.put("Subject", "getSubject");
		fieldMap.put("unid", "getUnid");
		fieldMap.put("UserAgent", "getUserAgent");
		fieldMap.put("Website", "getWebSite");
		return fieldMap;
	}
}
