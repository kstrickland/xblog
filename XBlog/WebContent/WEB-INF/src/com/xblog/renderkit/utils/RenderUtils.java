package com.xblog.renderkit.utils;

/*
 * This class is for handling various duties dealing with content replacement and building
 * individual HTML elements
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.EmbeddedObject;
import lotus.domino.NotesException;
import lotus.domino.RichTextItem;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;
import lotus.domino.ViewNavigator;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.ibm.xsp.component.UIInclude;
import com.ibm.xsp.component.UIScriptCollector;
import com.ibm.xsp.extlib.component.containers.UIList;
import com.xblog.components.Comment;
import com.xblog.components.Page;
import com.xblog.components.Post;
import com.xblog.components.SideBarItem;
import com.xblog.frontend.DateFormatter;
import com.xblog.interfaces.IEntry;
import com.xblog.query.XspQuery;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogUtils;

public class RenderUtils {

	/**
	 * Loop through all the entries in a view and replace the tag with the value
	 * listed
	 * 
	 * @param html
	 *            - the html of the post/page
	 * @param viewName
	 *            - the view name to use
	 * @param tagColumnNum
	 *            - The column number that contains the tags
	 * @param replaceColumnNum
	 *            - The column number that contains the values to replace the
	 *            tags with
	 * @return - String HTML
	 */
	public static String replaceHotTags(String html, String viewName, int tagColumnNum, int replaceColumnNum, IEntry entry) {
		try {
			Map<String, String> hotTags;
			// A little weirdness going on here, if I include hotText in the
			// hotTags map, it breaks the rendering
			if (viewName.equalsIgnoreCase("(luHotText)")) {
				hotTags = XBlogUtils.getCurrentInstance().getHotTextValues();
			} else {
				hotTags = XBlogUtils.getCurrentInstance().getHotTagValues();
			}
			for (Map.Entry<String, String> mapEntry : hotTags.entrySet()) {
				String tag = mapEntry.getKey();
				String replaceText = mapEntry.getValue();
				String blogUrl = XBlogUtils.getCurrentInstance().getBlogUrl();
				if (tag.equalsIgnoreCase("<$FullPermalink$>")) {
					String replaceBlogUrl = "";
					if (entry != null) {
						replaceBlogUrl = blogUrl + "/default.xsp?documentId=" + entry.getUnid();
					} else {
						replaceBlogUrl = blogUrl + "/default.xsp";
					}
					replaceText = replaceBlogUrl;
				} else if (tag.equalsIgnoreCase("<$BlogUrl$>")) {
					replaceText = blogUrl.toString();
				} else if (entry != null && tag.equalsIgnoreCase("<$NumberComments$>")) {
					replaceText = XBlogUtils.getCurrentInstance().getNumberComments(entry.getUnid()).toString();
				}
				if (html == null) {
					html = "";
				}
				html = html.replace(tag, replaceText);
			}
			return html;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Replaces all the translation tags on a page
	 * 
	 * @param html
	 *            - the html of the post/page
	 * @param viewName
	 *            - the view name to use to lookup the tags
	 * @param tagColumnNum
	 *            - The column number that contains the tags
	 * @param replaceColumnNum
	 *            - The column number that contains the values to replace the
	 *            tags with
	 * @param lookupKey
	 *            - The language lookup key (i.e. en, fr, de, etc.)
	 * @return String HTML
	 */
	public static String replaceHotTags(String html, String viewName, int tagColumnNum, int replaceColumnNum, String lookupKey, IEntry entry) {
		try {
			Map<String, String> translations = XBlogUtils.getCurrentInstance().getTranslationValues();
			for (Map.Entry<String, String> mapEntry : translations.entrySet()) {
				String tag = mapEntry.getKey();
				if (tag.equals("<$TransPostComment$>")) {
					/*
					 * Set the Text for the "Post Comment" link
					 */
					String parentUnid = JSFUtil.getParamValue("documentId");
					if (parentUnid != null && !parentUnid.isEmpty()) {
						Document parentDoc = NotesContext.getCurrent().getCurrentDatabase().getDocumentByUNID(
								JSFUtil.getParamValue("documentId"));
						if (parentDoc != null && !parentDoc.getItemValueString("allowComments").equalsIgnoreCase("Yes")) {
							String onClickScript = "XBlog.showHideComment('show')";
							String replaceLink = "<a class=\"PostCommentLink\" " + "onclick=\"" + onClickScript + "\">"
									+ mapEntry.getValue() + "</a>";
							html = html.replace(tag, replaceLink);
						} else {
							html = html.replace(tag, "");
						}
					}
				} else if (tag.equals("<$TransCommentsDisabled$>")) {
					/*
					 * If comments are disabled set the text for
					 * "Comments Disabled"
					 */
					if (entry != null && entry instanceof Post) {
						Post post = (Post) entry;
						if (!post.isCommentsDisabled()) {
							html = html.replace(tag, "");
						} else {
							html = html.replace(tag, mapEntry.getValue());
						}
					}
				} else if (tag.equals("<$TransReadMore$>")) {
					/*
					 * If we've got "Read More" content, add the widget
					 */
					if (entry != null && entry instanceof Post) {
						Post post = (Post) entry;
						if (post.getMimeReadMore() != null) {
							html = html.replace(tag, mapEntry.getValue());
						} else {
							html = html.replace(tag, "");
						}
					}
				} else if (tag.equals("<$TransNavigation$>")) {
					/*
					 * This is mainly a place holder of where we'll build the
					 * navigation structure when on a permalink page
					 */
					if (html.indexOf("<$TransNavigation$>") > -1) {
						String content = XBlogUtils.getCurrentInstance().getLayoutValue("contentContains", 0).toString();
						if (content.equalsIgnoreCase("blog") || content.equalsIgnoreCase("All Pages")) {
							String navHtml = buildPermalinkNavigation(entry);
							html = html.replace(tag, navHtml);
						} else {
							if (JSFUtil.getParamValue("type") != null && JSFUtil.getParamValue("type").toString().equalsIgnoreCase("page")) {
								String navHtml = buildPermalinkNavigation(entry);
								html = html.replace(tag, navHtml);
							}
						}
					}
				} else {
					/*
					 * If the tag isn't one of the specialized tags above, just
					 * do a straight replacement
					 */
					html = html.replace(tag, mapEntry.getValue());
				}
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return html;
	}

	/**
	 * This cleans all the remaining hot/translation tags out of the html in the
	 * case of blank/non-existent fields. It will not remove non-existent tags
	 * so that admins can tell when they mis-spelled something
	 * 
	 * @param html
	 *            - String - The html to clean
	 * @return String - The provided html with all unused tags cleaned out
	 */
	public static String cleanLeftoverTags(String html) {
		try {
			Map<String, String> allTags = new HashMap<String, String>();
			allTags.putAll(XBlogUtils.getCurrentInstance().getHotTagValues());
			allTags.putAll(XBlogUtils.getCurrentInstance().getTranslationValues());
			for (String key : allTags.keySet()) {
				String tag = key;
				/*
				 * We don't want to clean the comment form tags because the
				 * comment html may not have been built yet
				 */
				if (!key.equalsIgnoreCase("comment")) {
					html = html.replace(tag, "");
				}
			}
			return html;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This builds the html for the navigation that shows up at the top of the
	 * actual blog post/page
	 * 
	 * @param - IEntry - The entry we're working with
	 * @return String - The HTML for the permalink navigation structure
	 */
	public static String buildPermalinkNavigation(IEntry entry) {
		try {
			Database db = NotesContext.getCurrent().getCurrentDatabase();
			View blogView = null;
			if (entry instanceof Page) {
				blogView = db.getView("(homePagesByDate)");
			} else {
				blogView = db.getView("(homeBlogsByDate)");
			}
			String postDate = entry.getCreatedDate();
			DateTime nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(postDate);
			ViewEntry startingEnt = blogView.getEntryByKey(nDate);
			StringBuilder sb = new StringBuilder();
			sb.append("<center><b>Could not determine navigation structure</b></center>");
			if (startingEnt != null) {
				String startingPos = startingEnt.getPosition('.');
				ViewNavigator viewNav = blogView.createViewNav();
				viewNav.gotoPos(startingPos, '.');
				ViewEntry nextEnt = viewNav.getPrev();
				viewNav = blogView.createViewNavFrom(startingEnt);
				ViewEntry prevEnt = viewNav.getNext();
				String prevLink = "";
				String nextLink = "";
				sb.replace(0, sb.toString().length(), "<div class='PermalinkNavContainer'>");
				if (prevEnt != null) {
					if (entry instanceof Page) {
						prevLink = "<a href='" + XBlogUtils.getCurrentInstance().getBlogUrl() + "/default.xsp?documentId="
								+ prevEnt.getUniversalID() + "&type=page' title='" + prevEnt.getDocument().getItemValueString("Title")
								+ "'>" + prevEnt.getDocument().getItemValueString("Title") + "</a>";
					} else {
						prevLink = "<a href='" + XBlogUtils.getCurrentInstance().getBlogUrl() + "/default.xsp?documentId="
								+ prevEnt.getUniversalID() + "' title='" + prevEnt.getDocument().getItemValueString("Title") + "'>"
								+ prevEnt.getDocument().getItemValueString("Title") + "</a>";
					}
					sb.append("<span class='PermalinkNavPrev PermalinkNavItem'>" + prevLink + "</span>");
				}
				sb.append("<span class='PermalinkNavHome PermalinkNavItem'>");
				sb.append("<a href='" + XBlogUtils.getCurrentInstance().getRelativeUrl() + "/default.xsp'>Home</a>");
				sb.append("</span>");
				if (nextEnt != null) {
					if (entry instanceof Page) {
						nextLink = "<a href='" + XBlogUtils.getCurrentInstance().getBlogUrl() + "/default.xsp?documentId="
								+ nextEnt.getUniversalID() + "&type=page' title='" + nextEnt.getDocument().getItemValueString("Title")
								+ "'>" + nextEnt.getDocument().getItemValueString("Title") + "</a>";
					} else {
						nextLink = "<a href='" + XBlogUtils.getCurrentInstance().getBlogUrl() + "/default.xsp?documentId="
								+ nextEnt.getUniversalID() + "' title='" + nextEnt.getDocument().getItemValueString("Title") + "'>"
								+ nextEnt.getDocument().getItemValueString("Title") + "</a>";
					}
					sb.append("<span class='PermalinkNavNext PermalinkNavItem'>" + nextLink + "</span>");
				}
				sb.append("</div>");
			}
			return sb.toString();
		} catch (NotesException e) {
			e.printStackTrace();
			return "<center><b>Error in Determining Navigation</b></center>";
		} catch (Exception e) {
			e.printStackTrace();
			return "<center><b>Error in Determining Navigation</b></center>";
		}
	}

	/**
	 * replace all the Hot Text items
	 * 
	 * @param html
	 *            String - The html we want to search for the hot text to
	 *            replace. We should run this from the component and not the
	 *            renderer. This ensures we don't break title, tag, author, etc.
	 *            links that aren't part of the actual content of a blog post.
	 * @return - String - The html that was provided with all the occurences of
	 *         any hot text items replaced
	 */
	public static String replaceHotText(String html, IEntry entry) {
		if (XBlogUtils.getCurrentInstance().getConfigValue("EntryProcessHotText", 0).toString().equalsIgnoreCase("Yes")) {
			return replaceHotTags(html, "(luHotText)", 0, 1, entry);
		} else {
			return html;
		}
	}

	/**
	 * replace all the emoticon text with the emoticon from the application
	 * 
	 * @param html
	 *            - java.lang.String - The html to search for and replace
	 *            emoticon patterns
	 * @return - java.lang.String - The html that was provided with all the
	 *         occurences of any emoticons replaced with the image tag
	 */
	public static String replaceEmoticons(String html, IEntry entry) {
		if (XBlogUtils.getCurrentInstance().getConfigValue("EntryProcessEmoticons", 0).toString().equalsIgnoreCase("Yes")) {
			return replaceHotTags(html, "(luEmoticon)", 0, 1, entry);
		} else {
			return html;
		}
	}

	/**
	 * replace all the translated words
	 * 
	 * @param html
	 *            - java.lang.String - The html to search for translation tags
	 * @return - java.lang.String - The html that was provided with all the
	 *         occurences of any translated text replaced
	 */
	@SuppressWarnings("unused")
	public static String replaceTranslations(String html, IEntry entry) {
		String luKey = (String) XBlogUtils.getCurrentInstance().getConfigValue("Language", 0);
		return replaceHotTags(html, "(luTranslations)", 1, 2, luKey, entry);
	}

	/**
	 * Builds the HTML for the author link found throughout the site (i.e. blog
	 * posts, pages - Not comments)
	 * 
	 * @param com
	 *            .xblog.interfaces.IEntry - The entry we're working with
	 * @return java.lang.String - Link for the author
	 */
	public static String buildAuthorHtml(IEntry entry) {
		XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
		String authorHtml = "<a href='" + xbUtils.getBlogUrl() + "/default.xsp?author=" + entry.getAuthor() + "'>" + entry.getAuthor()
				+ "</a>";
		return authorHtml;
	}

	/**
	 * Format a date
	 * 
	 * @param IEntry
	 *            - the entry we're working with
	 * @return java.lang.String of the formatted date
	 */
	public static String formatDate(IEntry entry) {
		try {
			if (entry.getCreatedDate() != null) {
				XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
				String dateFormatStr = "MM/dd/yyyy hh:mm a";
				if (entry instanceof Post) {
					dateFormatStr = xbUtils.getConfigValue("PostDateFormat", 0).toString();
				} else if (entry instanceof Comment) {
					dateFormatStr = xbUtils.getConfigValue("CommentDateFormat", 0).toString();
				}
				String dateStr = entry.getCreatedDate();
				DateTime nDateValue = NotesContext.getCurrent().getCurrentSession().createDateTime(dateStr);
				// DateTime nDateValue = (DateTime) post.getCreatedDate();
				return DateFormatter.getFormattedDate(dateFormatStr, nDateValue.toJavaDate());
			}
			return "";
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Format a date
	 * 
	 * @param Date
	 *            - the date we're working with
	 * @return java.lang.String of the formatted date
	 */
	public static String formatRSSDate(Date formatDate) {
		try {
			String dateFormatStr = "EEE, dd MMM yyyy HH:mm:ss Z";
			return DateFormatter.getFormattedDate(dateFormatStr, formatDate);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Perform the bare minimum for HTML replacement
	 * 
	 * @param html
	 *            - String - The HTML to build
	 * @param entry
	 *            - IEntry - The entry we're working with
	 * @return - String - The built html
	 */
	public static String buildGenericHtml(String html, IEntry entry) {
		html = replaceHotTags(html, "(luCustomHotTags)", 0, 1, entry);
		html = replaceTranslations(html, entry);
		html = cleanLeftoverTags(html);
		return html;
	}

	/**
	 * Wrap the search term in a span with a style class so we can highlight it
	 * via CSS
	 * 
	 * @param html
	 *            - String - The html that would be returned because of the
	 *            search
	 * @param searchTerm
	 *            - String - What we're searching for
	 * @return String - The html with the search term highlighted
	 */
	public static String highLightSearchTerm(String html, String searchTerm) {
		if (searchTerm != null && !searchTerm.isEmpty()) {
			html = html.replace(searchTerm, "<span class='SearchTerm'>" + searchTerm + "</span>");
		}
		return html;
	}

	/**
	 * This will build all the published sidebars and inject them into the
	 * proper locations. This is called from the afterPageLoad event in the
	 * layoutDefault custom control. We call it there because if we call it from
	 * any of the methods here the sidebars get built multiple times (once for
	 * each entry)
	 */
	@SuppressWarnings("unchecked")
	public static void buildSideBars() {
		try {
			Map viewScope = (Map) JSFUtil.getVariableValue("viewScope");
			List<String> sideBarsArr;
			/*
			 * We record which sidebars have been built because we were having
			 * issues with sidebars being built multiple times
			 */
			if (viewScope.containsKey("sideBarsBuilt")) {
				sideBarsArr = (List) viewScope.get("sideBarsBuilt");
			} else {
				sideBarsArr = new ArrayList();
			}
			View sideBarView = JSFUtil.getCurrentDatabase().getView("(luSidebars)");
			ViewEntryCollection entCol = sideBarView.getAllEntries();
			ViewEntry ent = entCol.getFirstEntry();

			/*
			 * Loop through all the published sidebars and build a SideBarItem.
			 * We then inject that item into the page at run time in the proper
			 * location
			 */
			UIComponent rightSideContainer = null;
			UIComponent leftSideContainer = null;
			while (ent != null) {
				if (!sideBarsArr.contains(ent.getDocument().getUniversalID())) {
					SideBarItem sideBarItem = new SideBarItem();
					sideBarItem.setSideBarLocation(ent.getColumnValues().get(0).toString());
					sideBarItem.setSideBarTitle(ent.getColumnValues().get(2).toString());
					sideBarItem.setSideBarType(ent.getColumnValues().get(1).toString());
					sideBarItem.setSideBarItemId(ent.getDocument().getUniversalID());
					sideBarItem.setSideBarDocUNID(ent.getDocument().getUniversalID());
					if (ent.getColumnValues().get(1).toString().equalsIgnoreCase("html")) {
						sideBarItem.setSideBarContent(ent.getDocument().getFirstItem("contentRT").getMIMEEntity().getContentAsText());
					}
					if (sideBarItem.getSideBarType().equalsIgnoreCase("Downloads")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("Most Popular")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("Most Commented")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("Archives")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("Last Comments Posted")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("Now Viewing")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("Pages")
							|| sideBarItem.getSideBarType().equalsIgnoreCase("RSS Feed")) {
						int numItems = Integer.parseInt(ent.getColumnValues().get(5).toString());
						sideBarItem.setSideBarNumberItems(numItems);
					}
					if (sideBarItem.getSideBarType().equalsIgnoreCase("Archives")) {
						sideBarItem.setSideBarArchiveDateFormat(ent.getDocument().getFirstItem("DateFormat").getValueString());
					}
					if (sideBarItem.getSideBarType().equalsIgnoreCase("My Twitter")) {
						boolean showAvatar = true;
						boolean showScrollbar = false;
						if (!ent.getDocument().getItemValueString("ShowTwitterAvatar").equalsIgnoreCase("Yes")) {
							showAvatar = false;
						}
						if (ent.getDocument().getItemValueString("ShowTwitterScrollbar").equalsIgnoreCase("Yes")) {
							showScrollbar = true;
						}
						sideBarItem.setTwitterLinkColor(ent.getDocument().getItemValueString("TwitterLinkColor"));
						sideBarItem.setTwitterSidebarWidth(ent.getDocument().getItemValueString("TwitterSidebarWidth"));
						sideBarItem.setTwitterTweetColor(ent.getDocument().getItemValueString("TwitterTweetColor"));
						sideBarItem.setTwitterUser(ent.getDocument().getItemValueString("TwitterUser"));
						sideBarItem.setShowTwitterAvatar(showScrollbar);
						sideBarItem.setShowTwitterScrollbar(showAvatar);
						sideBarItem.setTwitterBackgroundColor(ent.getDocument().getItemValueString("TwitterShellBackground"));
					}
					if (sideBarItem.getSideBarType().equalsIgnoreCase("Poll")) {
						View pollView = NotesContext.getCurrent().getCurrentDatabase().getView("(luActivePoll)");
						Document pollDoc = pollView.getFirstDocument();
						if (pollDoc != null) {
							UIInclude include = new UIInclude();
							include.setPageName("ccPoll.xsp");
						}
					}
					if (sideBarItem.getSideBarType().equalsIgnoreCase("RSS Feed")) {
						sideBarItem.setFeedPortletUrl(ent.getDocument().getItemValueString("RSSUrl"));
					}
					UIComponent sidebarContainer = null;
					XspQuery query = new XspQuery();
					if (sideBarItem.getSideBarLocation().equalsIgnoreCase("Left")) {
						leftSideContainer = query.byId("LayoutLeftSidePanel");
						sidebarContainer = leftSideContainer;
					} else if (sideBarItem.getSideBarLocation().equalsIgnoreCase("Right")) {
						rightSideContainer = query.byId("LayoutRightSidePanel");
						sidebarContainer = rightSideContainer;
					}
					/*
					 * If this sidebar isn't in the sideBarsArr (viewScope
					 * variable) then inject it, else move along
					 */
					sideBarsArr.add(ent.getDocument().getUniversalID());
					if (sidebarContainer != null) {
						sidebarContainer.getChildren().add(sideBarItem);
					}
				}
				ent = entCol.getNextEntry(ent);
			}
			viewScope.put("sideBarsBuilt", sideBarsArr);
		} catch (NotesException e) {
			e.printStackTrace();
		}
		XspQuery query = new XspQuery();
		UIComponent blogContainer = query.byId("layoutInclude");
		addPartialRefreshScript(blogContainer.getClientId(FacesContext.getCurrentInstance()));
	}

	/**
	 * Add a partial refresh script to the page
	 * 
	 * @param refreshId
	 *            - The ID to partially refresh
	 */
	public static void addPartialRefreshScript(String refreshId) {
		UIScriptCollector.find().addScript("XSP.addOnLoad(function(){" + "XSP.partialRefreshPost('" + refreshId + "',{});" + "});");
	}

	/**
	 * Build the html for an attachment in the Mobile Interface
	 * 
	 * @param unid
	 *            - String - The UNID of the post/page which contains the
	 *            attachment(s)
	 * @return String - The HTML for a link or list of links
	 */
	public static String buildAttachmentHtml(String unid) {
		String attachmentHtml = "";
		try {
			Document doc = NotesContext.getCurrent().getCurrentDatabase().getDocumentByUNID(unid);
			String blogUrl = XBlogUtils.getCurrentInstance().getBlogUrl();
			RichTextItem rtItem = (RichTextItem) doc.getFirstItem("attachments");
			if (rtItem != null) {
				Vector<EmbeddedObject> eoList = rtItem.getEmbeddedObjects();
				if (eoList.size() > 0) {
					for (EmbeddedObject eo : eoList) {
						attachmentHtml = attachmentHtml + "<a href='" + blogUrl + "/0/" + unid + "/$FILE/" + eo.getName() + "'>"
								+ eo.getName() + "</a><br>";
					}
				}
			} else {
				// System.out.println("No embedded Objects");
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return attachmentHtml;
	}

	/**
	 * Build the HTML for a div with a widget to show/hide the content of the
	 * div. This is dependent on the frontEndRpc method showHideReadMore(unid)
	 * 
	 * @param html
	 *            String - The html that is to be shown/hidden
	 * @return XspDiv - The div to place on the page with the show/hide widget
	 */
	public static String buildHideShowDiv(String html, String unid) {
		try {
			String unidStr = "\"" + unid + "\"";
			String readMoreText = "<div class='HideShowTopDiv'><span class='HideShowLinkImg'>";
			readMoreText = readMoreText + "<div id='" + unid + "' class='HideShowInnerDiv' style='display: none;'>" + html + "</div>";
			readMoreText = readMoreText + "<a onclick='XBlog.showHideReadMore(" + unidStr + ")' id='ShowHideLink-" + unid
					+ "' class='ShowHideExpand'><$TransReadMore$></a></span>";
			readMoreText = readMoreText + "</div>";
			return readMoreText;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}

	public static void buildContentArea(String html) {

	}

	public static String getDefaultHtml(String type, String fieldName) {
		Object value = null;
		String defaultHtml = "";
		if (type.equalsIgnoreCase("layout")) {
			value = XBlogUtils.getCurrentInstance().getLayoutValue(fieldName);
		} else if (type.equalsIgnoreCase("config")) {
			value = XBlogUtils.getCurrentInstance().getConfigValue(fieldName);
		}
		if (value != null) {
			if (value instanceof Vector) {
				Vector valueVect = (Vector) value;
				defaultHtml = XBlogUtils.getCurrentInstance().vectorToString(valueVect);
			} else {
				defaultHtml = value.toString();
			}
		}
		return defaultHtml;
	}

	public static UIList buildList(List<String> listItems) {

		return null;
	}

	public static String getTranslationText(String tag) {
		String result = "";
		try {
			Database db = NotesContext.getCurrent().getCurrentDatabase();
			View transView = db.getView("(luTranslationsTag)");
			String lang = XBlogUtils.getCurrentInstance().getConfigValue("Language", 0).toString();
			String luKey = lang.toUpperCase() + tag;
			ViewEntry ent = transView.getEntryByKey(luKey);
			if (ent != null) {
				result = ent.getColumnValues().get(1).toString();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Create an MD5 hash of the email address and build the url to the Gravatar
	 * 
	 * @param email
	 *            - String - email address
	 * @return String - The image url to the gravatar image
	 */
	public static String getGravatarUrl(String email) {
		try {
			String url = "http://www.gravatar.com/avatar/";
			email = email.trim();
			String hash = MD5Util.md5Hex(email);
			url = url + hash + ".png?r=g";
			return url;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
