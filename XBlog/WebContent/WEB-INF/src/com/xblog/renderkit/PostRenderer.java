package com.xblog.renderkit;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;

import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.components.Post;
import com.xblog.renderkit.utils.RenderUtils;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogUtils;

public class PostRenderer extends Renderer {

	private String postHtml;
	private Map<String, Object> hotTags;

	@Override
	public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
		super.encodeBegin(context, component);
		Post post = (Post) component;
		postHtml = post.getDefaultHtml();
		hotTags = getHotTagMap(post);
		for (String replaceTag : hotTags.keySet()) {
			if (hotTags.get(replaceTag) != null) {
				postHtml = postHtml.replace(replaceTag, hotTags.get(replaceTag).toString());
			}
		}
		postHtml = RenderUtils.replaceHotTags(postHtml, "(luCustomHotTags)", 0, 1, post);
		postHtml = RenderUtils.replaceEmoticons(postHtml, post);
		postHtml = RenderUtils.replaceTranslations(postHtml, post);
		postHtml = RenderUtils.cleanLeftoverTags(postHtml);
		if (JSFUtil.getParamValue("search") != null && !JSFUtil.getParamValue("search").isEmpty()) {
			postHtml = RenderUtils.highLightSearchTerm(postHtml, JSFUtil.getParamValue("search"));
		}
	}

	@Override
	public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
		ResponseWriter writer = context.getResponseWriter();
		writer.append(postHtml);
		super.encodeEnd(context, component);
	}

	private String buildAttachmentHtml(Post post) {
		StringBuilder sb = new StringBuilder();		
		if (post.getAttachments() != null) {
			sb.append("<div class='AttachmentContainer'>");
			for (String attach : post.getAttachments()) {
				sb.append("<span class='BlogAttachmentLink'>");
				sb.append("<a href='" + attach + "'>" + attach.substring(attach.lastIndexOf('/') + 1) + "</a>");
				sb.append("</span>");
			}
			sb.append("</div>");
		}
		return sb.toString();
	}

	private String buildTagsHtml(Post post) {
		if (post.getTags() != null) {
			XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
			String[] tagList = post.getTags().split(",");
			StringBuilder sb = new StringBuilder();
			for (String tag : tagList) {
				String link = "<a href='" + xbUtils.getBlogUrl() + "/default.xsp?tag=" + tag.trim() + "'>" + tag.trim() + "</a>";
				sb.append("<span class='TagItem'>" + link + "</span>");
			}
			return sb.toString();
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getHotTagMap(Post post) {
		try {
			Map<String, Object> hotTagMap = new HashMap<String, Object>();
			Map<String, String> fieldMap = getFieldPropertyMap();
			Class klass = post.getClass();
			View hotView = NotesContext.getCurrent().getCurrentDatabase().getView("hottags");
			ViewEntryCollection entCol = hotView.getAllEntriesByKey("Post");
			ViewEntry ent = entCol.getFirstEntry();
			while (ent != null) {
				String field = ent.getColumnValues().get(2).toString().trim();
				String tag = ent.getColumnValues().get(1).toString().trim();
				String value = null;
				if (field.equalsIgnoreCase("author")) {
					value = RenderUtils.buildAuthorHtml(post);
				} else if (field.equalsIgnoreCase("tags")) {
					value = buildTagsHtml(post);
				} else if (field.equalsIgnoreCase("attachments")) {
					if (post.getAttachments() != null) {
						value = buildAttachmentHtml(post);
					} else {
						value = "";
					}
				} else if (field.equalsIgnoreCase("createdDate")) {
					value = RenderUtils.formatDate(post);
				} else if (field.equalsIgnoreCase("postContent")) {
					value = post.getMimeContent();
					value = RenderUtils.replaceEmoticons(value, post);
				} else if (field.equalsIgnoreCase("readMoreContent")) {
					if (post.getMimeReadMore() != null && !post.getMimeReadMore().isEmpty()) {
						value = RenderUtils.replaceHotText(post.getMimeReadMore(), post);
						value = RenderUtils.replaceEmoticons(value, post);
						if (tag.equalsIgnoreCase("<$HiddenReadMore$>")) {
							value = RenderUtils.buildHideShowDiv(value, post.getUnid());
						}
					}
				} else {
					Object tempValue = klass.getMethod(fieldMap.get(field)).invoke(post);
					if (tempValue != null) {
						if (tempValue instanceof Integer) {
							value = ((Integer) tempValue).toString();
						} else {
							value = tempValue.toString();
						}
					} else {
						value = "";
					}
				}
				hotTagMap.put(tag, value);
				ent = entCol.getNextEntry(ent);
			}
			return hotTagMap;
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Map<String, String> getFieldPropertyMap() {
		Map<String, String> fieldMap = new HashMap<String, String>();
		fieldMap.put("author", "getAuthor");
		fieldMap.put("attachments", "getAttachments");
		fieldMap.put("allowComments", "isCommentsDisabled");
		fieldMap.put("createdDate", "getCreatedDate");
		fieldMap.put("department", "getDepartment");
		fieldMap.put("OldPermalink", "getFriendlyUrl");
		fieldMap.put("location", "getLocation");
		fieldMap.put("postContent", "getMimeContent");
		fieldMap.put("readMoreContent", "getMimeReadMore");
		fieldMap.put("numberComments", "getNumberComments");
		fieldMap.put("permalink", "getPermalink");
		fieldMap.put("rssAbstract", "getTextComment");
		fieldMap.put("rssReadMore", "getTextReadMore");
		fieldMap.put("title", "getTitle");
		fieldMap.put("viewNumber", "getViewed");
		fieldMap.put("CommentsPermalink", "getCommentsPermalink");
		return fieldMap;
	}
}
