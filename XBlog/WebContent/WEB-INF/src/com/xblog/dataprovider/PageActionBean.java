package com.xblog.dataprovider;

import java.io.Serializable;

import com.ibm.xsp.model.DataObject;
import com.xblog.interfaces.IPageAction;

public class PageActionBean implements DataObject, Serializable {

	public Class<?> getType(Object arg0) {
		return IPageAction.class;
	}

	public Object getValue(Object arg0) {
		if (arg0.equals("save")) {
			return new PageSaveAction();
		}
		return null;
	}

	public boolean isReadOnly(Object arg0) {
		return true;
	}

	public void setValue(Object arg0, Object arg1) {
	}

}
