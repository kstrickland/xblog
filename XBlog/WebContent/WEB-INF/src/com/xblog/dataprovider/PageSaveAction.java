package com.xblog.dataprovider;

import java.io.Serializable;

public class PageSaveAction extends PageActionRedirect implements Serializable {

	public Object execute(String formName) {
		DataProvider dataProvider = DataProvider.getCurrentInstance();
		if (dataProvider != null) {
			if (formName != null && !formName.equals("")) {
				dataProvider.save(formName);
			} else {
				dataProvider.save();
			}
		}
		return null;
	}

	@Override
	public Object execute() {
		execute("");
		return null;
	}

}
