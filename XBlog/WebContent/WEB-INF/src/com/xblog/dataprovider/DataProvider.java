package com.xblog.dataprovider;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.EmbeddedObject;
import lotus.domino.Item;
import lotus.domino.MIMEEntity;
import lotus.domino.NotesException;
import lotus.domino.RichTextItem;
import lotus.domino.Stream;
import lotus.domino.View;
import lotus.domino.ViewEntryCollection;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.ibm.xsp.component.UIScriptCollector;
import com.ibm.xsp.component.UIFileuploadEx.UploadedFile;
import com.ibm.xsp.designer.context.ServletXSPContext;
import com.ibm.xsp.designer.context.XSPUrl;
import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.ibm.xsp.http.IUploadedFile;
import com.ibm.xsp.http.MimeMultipart;
import com.xblog.datamaintainer.LayoutMaintainer;
import com.xblog.interfaces.IDataObjectEx;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogUtils;

/**
 * This bean was originally from :
 * http://www.mindoo.com/web/blog.nsf/dx/18.03.2011104725
 * KLEDH8.htm?opendocument&comments#anc1 and has been modified to handle data
 * for XBlog. This bean handles all data retrieving and saving for all XBlog
 * forms which reside in the backend.
 */

public class DataProvider implements IDataObjectEx, Serializable {

	public Map<String, Object> cachedValues;
	public Map<String, Object> changedValues;
	private String docUNID;
	private boolean hasImage = false;
	private String parentUNID;
	private Document curDoc;

	/**
	 * DataProvider Constructor - we setup the cachedValues and changedValues
	 * Maps
	 */
	public DataProvider() {
		XBlogUtils.getCurrentInstance().dbar.info("Got a DataProvider bean", "DataProvider()");
		cachedValues = new HashMap<String, Object>();
		changedValues = new HashMap<String, Object>();
	}

	/**
	 * Get the current instance of the DataProvider bean
	 * 
	 * @return DataProvider - the current instance
	 */
	public static DataProvider getCurrentInstance() {
		return (DataProvider) JSFUtil.getVariableValue("data");
	}

	/**
	 * Get the UNID of the document we're currently processing
	 * 
	 * @return String - lotus.domino.Document.UniversalID
	 */
	@SuppressWarnings("unchecked")
	public String getDocUNID() {
		if (docUNID == null) {
			Map paramValues = (Map) JSFUtil.getVariableValue("paramValues");
			String[] documentIdArr = (String[]) paramValues.get("documentId");
			if (documentIdArr == null || documentIdArr.length == 0) {
				String[] documentUNIDArr = (String[]) paramValues.get("unid");
				if (documentUNIDArr == null || documentUNIDArr.length == 0) {
					docUNID = "";
				} else {
					docUNID = documentUNIDArr[0];
				}
			} else {
				docUNID = documentIdArr[0];
			}
		}
		return docUNID;
	}

	/**
	 * Get the document object we're currently processing
	 * 
	 * @return lotus.domino.Document - the currentDocument we're processing
	 * @throws NotesException
	 */
	public Document getDocument() throws NotesException {
		String documentUNID = getDocUNID();
		if (documentUNID.equals("") || documentUNID == null) {
			return null;
		} else {
			Database db = ExtLibUtil.getCurrentDatabase();
			Document doc = db.getDocumentByUNID(documentUNID);
			return doc;
		}
	}

	/**
	 * Get the notesItem we're currently processing and convert the rich text
	 * from the RichText Editor, convert it to html and set the value of the
	 * rich text field
	 * 
	 * @param doc
	 *            - Document - The document we're currently editing/saving
	 * @param notesItem
	 *            - The notesItem we're currently processing
	 * @param itemValue
	 *            - The html representation of the MIME data in the notesItem
	 * @throws NotesException
	 */
	private void writeRichText(Document doc, Item notesItem, String itemValue) throws NotesException {
		if (notesItem != null) {
			MIMEEntity entity = notesItem.getMIMEEntity();
			if (entity == null) {
				if (doc == null) {
					// TODO: probably need to throw an error here as several
					// things have went wrong already
					return;
				} else {
					String itemName = notesItem.getName();
					doc.removeItem(itemName);
					entity = doc.createMIMEEntity(itemName);
				}
			}
			Stream stream = NotesContext.getCurrent().getCurrentSession().createStream();
			stream.writeText(itemValue);
			entity.setContentFromText(stream, "text/html;charset=UTF-8", entity.ENC_IDENTITY_7BIT);
		}
	}

	/**
	 * Get the notesItem we're currently processing and convert it to a Vector
	 * and then replace the itemValues with the vector
	 * 
	 * @param notesItem
	 *            - The notesItem we're currently processing
	 * @param itemValue
	 *            - The comma delimited string to convert to a vector
	 * @throws NotesException
	 */
	private void writeMultiValues(Item notesItem, String itemValue) throws NotesException {
		if (itemValue.contains(",")) {
			String[] itemValues = itemValue.split(",");
			Vector values = new Vector(Arrays.asList(itemValues));
			notesItem.setValues(values);
		}
	}

	/**
	 * Write the uploaded file to the document
	 * 
	 * @param notesItem
	 *            - RichTextItem = The rich text item that the file will be
	 *            stored in
	 * @param uploadedFile
	 *            IUploadedFile = the uploaded file
	 * @throws NotesException
	 */
	private void writeUploadedFile(RichTextItem notesItem, IUploadedFile uploadedFile) throws NotesException {
		File fileTemp = uploadedFile.getServerFile();
		File fileCorrected = new File(fileTemp.getParentFile().getAbsolutePath() + File.separator + uploadedFile.getClientFileName());
		fileTemp.renameTo(fileCorrected);
		notesItem.embedObject(EmbeddedObject.EMBED_ATTACHMENT, null, fileCorrected.getAbsolutePath(), null);
		if (notesItem.getName().equalsIgnoreCase("imagefile")) {
			updateCachedValue("Name", uploadedFile.getClientFileName());
			writeItemValue(getDocument(), "Name", uploadedFile.getClientFileName());
		}
	}

	/**
	 * Does this document have an image. Used mainly for the image form
	 * 
	 * @return boolean
	 */
	public boolean isHasImage() {
		return hasImage;
	}

	/**
	 * Set the permalink field if needed
	 * 
	 * @param doc
	 *            - the current document
	 */
	private boolean writePermaLink(Document doc) throws NotesException {
		String thisUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
		String permaLink = null;
		if (doc.getItemValueString("Form").equalsIgnoreCase("Post") || doc.getItemValueString("Form").equalsIgnoreCase("Page")) {
			if (getValue("OldPermalink") != null && !getValue("OldPermalink").toString().isEmpty()) {
				permaLink = thisUrl + "/dx/" + getValue("OldPermalink").toString();
			} else {
				permaLink = thisUrl + "/default.xsp" + "?documentId=" + doc.getUniversalID();
			}
			doc.replaceItemValue("Permalink", permaLink);
			doc.replaceItemValue("CommentsPermalink", permaLink + "#comments");
			return true;
		} else if (doc.getItemValueString("Form").equalsIgnoreCase("Comment")) {
			if (getValue("OldPermalink") != null && !getValue("OldPermalink").toString().isEmpty()) {
				permaLink = thisUrl + "/dx/" + getValue("OldPermalink").toString() + "#" + getCommentNumber(doc.getParentDocumentUNID());
			} else {
				permaLink = thisUrl + "/default.xsp" + "?documentId=" + doc.getParentDocumentUNID() + "#"
						+ getCommentNumber(doc.getParentDocumentUNID());
			}
			doc.replaceItemValue("Permalink", permaLink);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Write alternative values to the image form
	 * 
	 * @param doc
	 * @throws NotesException
	 */
	private void writeImageValues(Document doc) throws NotesException {
		String thisUrl = XBlogUtils.getCurrentInstance().getBlogUrl();
		String attName = (String) getValue("Name");
		if (attName == null) {
			attName = doc.getItemValueString("Name");
		}
		String attachmentUrl = thisUrl + "/0/" + doc.getUniversalID() + "/$FILE/" + attName;

		String imgTag = "<img src='" + attachmentUrl + "' alt='" + attName + "'>";
		doc.replaceItemValue("URL", attachmentUrl);
		doc.replaceItemValue("IMGTag", imgTag);
	}

	/**
	 * Clear all the values in cachedValues and changedValues
	 */
	private void clearAllValues() {
		cachedValues.clear();
		changedValues.clear();
	}

	/**
	 * Write a value to a notes item. Called from the save(String) method for
	 * each field listed in changedValues
	 * 
	 * @param doc
	 *            - NotesDocument - The document we're currently editing/saving
	 * @param itemName
	 *            - String - The name of the field we want to update
	 * @param itemValue
	 *            - Object - The value to update the field with
	 */
	private void writeItemValue(Document doc, String itemName, Object itemValue) {
		try {
			if (itemValue == null) {
				doc.removeItem(itemName);
			} else {
				if (doc == null) {
					doc = curDoc;
				}
				Item notesItem = doc.getFirstItem(itemName);
				if (notesItem != null) {
					if ((notesItem.getType() == Item.MIME_PART || notesItem.getType() == Item.HTML || notesItem.getType() == Item.RICHTEXT)
							&& (!(itemValue instanceof UploadedFile))) {
						MimeMultipart rtValue = (MimeMultipart) itemValue;
						writeRichText(doc, notesItem, rtValue.getHTML());
					} else if (notesItem.getType() == Item.ATTACHMENT) {
						UploadedFile upFile = (UploadedFile) itemValue;
						IUploadedFile uploadedFile = upFile.getUploadedFile();
						writeUploadedFile((RichTextItem) notesItem, uploadedFile);
						if (doc.getItemValueString("Form").equalsIgnoreCase("emoticon")) {
							doc.replaceItemValue("Name", uploadedFile.getClientFileName());
							changedValues.put("Name", uploadedFile.getClientFileName());
						}
					} else if (notesItem.getType() == Item.DATETIMES) {
						if (itemValue instanceof Date) {
							DateTime nDate = NotesContext.getCurrent().getCurrentSession().createDateTime((Date) itemValue);
							doc.replaceItemValue(itemName, nDate);
						} else if (itemValue instanceof DateTime) {
							doc.replaceItemValue(itemName, itemValue);
						} else {
							// System.out.println("Don't know what type of value this is");
						}
					} else if (itemValue instanceof UploadedFile) {
						UploadedFile upFile = (UploadedFile) itemValue;
						IUploadedFile uploadedFile = upFile.getUploadedFile();
						RichTextItem notesRTItem = (RichTextItem) notesItem;
						writeUploadedFile(notesRTItem, uploadedFile);
						if (doc.getItemValueString("Form").equalsIgnoreCase("emoticon")) {
							doc.replaceItemValue("Name", uploadedFile.getClientFileName());
							changedValues.put("Name", uploadedFile.getClientFileName());
						}
					} else if (itemValue instanceof ArrayList) {
						ArrayList itemValueArr = (ArrayList) itemValue;
						Vector newItemValue = new Vector(itemValueArr);
						doc.replaceItemValue(itemName, newItemValue);
					} else {
						doc.replaceItemValue(itemName, itemValue);
					}
				} else {
					if (itemValue instanceof MimeMultipart) {
						MimeMultipart rtValue = (MimeMultipart) itemValue;
						notesItem = doc.createRichTextItem(itemName);
						writeRichText(doc, notesItem, rtValue.getHTML());
					} else if (itemValue instanceof Long || itemValue instanceof Double) {
						notesItem = doc.replaceItemValue(itemName, itemValue);
					} else if (itemValue instanceof String) {
						doc.replaceItemValue(itemName, itemValue);
					} else if (itemValue instanceof UploadedFile) {
						UploadedFile upFile = (UploadedFile) itemValue;
						IUploadedFile uploadedFile = upFile.getUploadedFile();
						RichTextItem notesRTItem = doc.createRichTextItem(itemName);
						writeUploadedFile(notesRTItem, uploadedFile);
						if (doc.getItemValueString("Form").equalsIgnoreCase("emoticon")) {
							doc.replaceItemValue("Name", uploadedFile.getClientFileName());
							changedValues.put("Name", uploadedFile.getClientFileName());
						}
					} else if (itemValue instanceof Date || itemValue instanceof DateTime) {
						Date javaDate;
						DateTime nDate;
						String itemValueStr = itemValue.toString();
						// Dunno why we're having to do this, it should be
						// caught by the first null check
						// for some reason it's not seeing the itemValue as null
						// until we turn it into a string
						if (itemValue != null && itemValueStr != null) {
							if (itemValue instanceof DateTime) {
								nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(itemValue.toString());
							} else {
								javaDate = (Date) itemValue;
								nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(javaDate);
							}
						} else {
							javaDate = new Date();
							nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(javaDate);
						}
						doc.replaceItemValue(itemName, nDate);
					} else if (itemValue instanceof ArrayList) {
						ArrayList itemValueArr = (ArrayList) itemValue;
						Vector newItemValue = new Vector(itemValueArr);
						doc.replaceItemValue(itemName, newItemValue);
					} else {
						doc.replaceItemValue(itemName, itemValue);
					}
					updateCachedValue(itemName, itemValue);
				}
			}
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update/add a value to the cachedValues map
	 * 
	 * @param keyName
	 *            - String - the fieldname we want to update
	 * @param itemValue
	 *            - Object - the value of the field
	 */
	private void updateCachedValue(String keyName, Object itemValue) {
		if (changedValues.containsKey(keyName)) {
			cachedValues.put(keyName, itemValue);
		}
	}

	/**
	 * Delete an attachment from a document
	 * 
	 * @param itemName
	 *            String = The field name where the file is stored
	 * @param fileName
	 *            String = The file name to delete
	 */
	public void deleteAttachment(String itemName, String fileName) {
		Document doc;
		try {
			doc = getDocument();
			if (doc != null) {
				Item notesItem = doc.getFirstItem(itemName);
				if (notesItem != null && notesItem instanceof RichTextItem) {
					RichTextItem notesRTItem = (RichTextItem) notesItem;
					EmbeddedObject eo = notesRTItem.getEmbeddedObject(fileName);
					if (eo != null) {
						eo.remove();
						doc.save(true, false);
						UIComponent refreshTarget = JSFUtil.findComponent("attachmentHolder");
						if (refreshTarget != null) {
							UIScriptCollector.find().addScript(
									"XSP.addOnLoad(function(){" + "XSP.partialRefreshGet('"
											+ refreshTarget.getClientId(FacesContext.getCurrentInstance()) + "');" + "});");
						}
					}
				}
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	public void redirectToSavedDoc(String unid) {
		try {
			ServletXSPContext thisContext = (ServletXSPContext) JSFUtil.getVariableValue("context");
			XSPUrl thisUrl = thisContext.getUrl();
			thisUrl.removeAllParameters();
			FacesContext.getCurrentInstance().getExternalContext().redirect(thisUrl + "?documentId=" + unid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Update values specific to the Comment form
	 * 
	 * @param doc
	 * @throws NotesException
	 */
	private void updateCommentValues(Document doc) throws NotesException {
		XBlogUtils.getCurrentInstance().dbar.info("updateCommentValues running", "DataProvider.updateCommentValues(Document)");
		Map sessionScope = (Map) JSFUtil.getVariableValue("sessionScope");
		String parentUnid = null;
		if (sessionScope.containsKey("parentUNID")) {
			parentUnid = sessionScope.get("parentUNID").toString();
		}
		if (parentUnid == null) {
			Map paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			String paramDocId = null;
			if (paramMap.containsKey("documentId")) {
				paramDocId = paramMap.get("documentId").toString();
			} else {
				paramDocId = this.parentUNID;
			}
			if (paramDocId != null && paramDocId.equalsIgnoreCase(doc.getUniversalID())) {
				parentUnid = doc.getParentDocumentUNID();
			} else {
				parentUnid = paramDocId;
			}
		}
		Document parentDoc = NotesContext.getCurrent().getCurrentDatabase().getDocumentByUNID(parentUnid);
		if (parentDoc != null) {
			doc.replaceItemValue("ParentUNID", parentUnid);
			int commentNumber = getCommentNumber(parentUnid);
			doc.replaceItemValue("CommentNumber", commentNumber);
			parentDoc.replaceItemValue("numberComments", commentNumber);
			doc.makeResponse(parentDoc);
			parentDoc.save(true, false);
			doc.save(true, true);
		}
		if (sessionScope.containsKey("parentUNID")) {
			sessionScope.remove("parentUNID");
		}
	}

	/**
	 * Get the number of comments for a post
	 * 
	 * @param parentUNID
	 * @return
	 * @throws NotesException
	 */
	private int getCommentNumber(String parentUNID) throws NotesException {
		XBlogUtils.getCurrentInstance().dbar.info("parentUNID = " + parentUNID, "DataProvider.getCommentNumber(String)");
		View commentView = NotesContext.getCurrent().getCurrentDatabase().getView("(luComments)");
		ViewEntryCollection entCol = commentView.getAllEntriesByKey(parentUNID);
		if (entCol.getCount() > 0) {
			return entCol.getCount() + 1;
		} else {
			return 1;
		}
	}

	@Deprecated
	private void pingServices() {
		try {
			Vector ping = XBlogUtils.getCurrentInstance().getConfigDoc().getItemValue("PingServices");
			if (ping != null && ping.size() > 0) {
				String url = "http://www.pingomatic.com/ping/?title="
						+ XBlogUtils.getCurrentInstance().getConfigValue("BlogName", 0).toString();
				url = url + "&blogurl=" + XBlogUtils.getCurrentInstance().getBlogUrl();
				url = url + "&rssurl=" + XBlogUtils.getCurrentInstance().getBlogUrl() + "/NewsFeed.xsp";
				for (int i = 0; i < ping.size(); i++) {
					url = url + "&" + ping.get(i) + "=on";
				}
				String script = "XSP.addOnLoad(function() {console.log('got here');window.open('" + url + "');});";
				UIScriptCollector scriptCollector = UIScriptCollector.find();
				scriptCollector.addScript(script);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/************** Interface Methods *************/
	/**
	 * Save the Document we're currently working with in the browser
	 */
	public void save(String formName) {
		XBlogUtils.getCurrentInstance().dbar.info("formName = " + formName, "DataProvider.save(String)");
		if (!changedValues.isEmpty()) {
			try {
				boolean isNewDoc = false;
				Document doc = getDocument();
				if (doc == null) {
					isNewDoc = true;
					Database db = ExtLibUtil.getCurrentDatabase();
					doc = db.createDocument();
					if (formName != null || !formName.equals("")) {
						doc.replaceItemValue("Form", formName);
					} else {
						doc.replaceItemValue("Form", "Post");
					}
				}
				curDoc = doc;
				if (isNewDoc && formName.equalsIgnoreCase("post")) {
					changedValues.put("numberComments", 0);
				}
				for (String itemName : changedValues.keySet()) {
					Object itemValue = changedValues.get(itemName);
					if (itemValue instanceof UploadedFile && formName.equalsIgnoreCase("image")) {
						hasImage = true;
					}
					writeItemValue(doc, itemName, itemValue);
				}
				/**
				 * If we're dealing with a comment then make it a response
				 * document
				 */
				if (formName.equalsIgnoreCase("Comment")) {
					updateCommentValues(doc);
				} else {
					doc.save(true, false);
				}
				String UNID = doc.getUniversalID();
				XBlogUtils.getCurrentInstance().dbar.info("Saving " + formName + " with a unid of " + UNID, "DataProvider.save(String)");
				/**
				 * Since the doc hadn't been saved when we added the image the
				 * unid wasn't present yet, so we need to set the url and ImgTag
				 * here since we should have a unid now
				 */
				if (isHasImage() && changedValues.containsKey("ImageFile") && formName.equalsIgnoreCase("Image")) {
					writeImageValues(doc);
					doc.save(true, false);
				}
				/**
				 * Write the permalink, includes the unid so gotta do it here
				 */
				if (writePermaLink(doc) && !formName.equalsIgnoreCase("Comment")) {
					doc.save(true, false);
				} else if (writePermaLink(doc) && formName.equalsIgnoreCase("Comment")) {
					doc.save(true, true);
				}

				/**
				 * We need to update some things if we modified the layout
				 */
				XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
				if (formName.equalsIgnoreCase("configuration")) {
					if (!xbUtils.getLayoutValue("layoutName", 0).toString().equalsIgnoreCase(getValue("Layout").toString())) {
						LayoutMaintainer.unPublishCurrentLayout(LayoutMaintainer.getLayoutUnid(getValue("Layout").toString()));
						LayoutMaintainer.setLayoutPublished(getValue("Layout").toString());
					}
				} else if (formName.equalsIgnoreCase("layout")) {
					if (!xbUtils.getLayoutValue("layoutName", 0).toString().equalsIgnoreCase(getValue("layoutName").toString())
							&& getValue("publish").toString().equalsIgnoreCase("yes")) {
						LayoutMaintainer.unPublishCurrentLayout(UNID);
						Document configDoc = JSFUtil.getCurrentDatabase().getDocumentByUNID(xbUtils.getConfigValue("unid").toString());
						configDoc.replaceItemValue("Layout", getValue("layoutName"));
						configDoc.save();
					}
				}

				if (isNewDoc) {
					redirectToSavedDoc(UNID);
				}
			} catch (NotesException e) {
				e.printStackTrace();
			}
			clearAllValues();
		} else {
			// System.out.println("com.xblog.dataprovider.DataProvider.changedValues is empty");
		}
	}

	public void save() {
		save("");
	}

	/**
	 * This should only be called from the posting of a comment from the
	 * frontend
	 * 
	 * @param formName
	 *            String - the name of the form
	 * @param parentUNID
	 *            String - The unid of the parent
	 */
	public void save(String formName, String parentUNID) {
		this.parentUNID = parentUNID;
		save(formName);
	}

	/**
	 * Get the java Type for the field we're currently processing
	 */
	public Class<?> getType(Object fieldName) {
		return fieldName.getClass();
	}

	/**
	 * This is actually called by the JSF engine for each field to return that
	 * field's value. We then add that value to the cachedValues Map
	 * 
	 * @param itemName
	 *            - The name of the field you want the value from
	 * @return Object - This should actually always be a string
	 */
	public Object getValue(Object itemName) {
		// XBlogUtils.getCurrentInstance().dbar.debug("itemName = " +
		// itemName.toString(), "DataProvider.getValue(Object)");
		String idStr = itemName.toString();
		if (idStr.equalsIgnoreCase("KeepAliveField")) {
			// System.out.println("Ping");
		}
		if (changedValues.containsKey(idStr)) {
			return changedValues.get(idStr);
		} else if (cachedValues.containsKey(idStr)) {
			return cachedValues.get(idStr);
		} else {
			try {
				if ((getDocument() == null && curDoc == null) || getDocUNID().equals("")) {
					return null;
				} else {
					Document doc = getDocument();
					Item notesItem = doc.getFirstItem(itemName.toString());
					if (notesItem != null) {
						Object itemValue = null;
						if (notesItem.getType() == Item.MIME_PART) {
							itemValue = notesItem.getMIMEEntity().getContentAsText();
						} else if (notesItem.getType() == Item.NUMBERS) {
							itemValue = notesItem.getValueDouble();
						} else if (notesItem.getType() == Item.DATETIMES) {
							itemValue = notesItem.getDateTimeValue().toJavaDate();
						} else if (notesItem.getType() == Item.NAMES) {
							// TODO - maybe not, we never get here
						} else if (notesItem.getType() == Item.ATTACHMENT) {
							// TODO - maybe not, we never get here
							// System.out.println("getValue for " +
							// itemName.toString() +
							// " item is Item.ATTACHMENT");
						} else {
							if (!notesItem.getValues().isEmpty()) {
								if (notesItem.getValues().size() > 1) {
									itemValue = notesItem.getValues().toArray();
								} else {
									itemValue = notesItem.getValueString();
								}
							}
						}
						cachedValues.put(doc.getFirstItem(itemName.toString()).getName(), itemValue);
						return itemValue;
					}
				}
			} catch (NotesException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				return null;
			}
		}
		return null;
	}

	/**
	 * Check if a field is read only
	 */
	public boolean isReadOnly(Object itemName) {
		return false;
	}

	/**
	 * This is actually called by the JSF engine to set the value for each field
	 * to what is currently on the page. We then add that value to the
	 * changedValues Map.
	 * 
	 * @param itemName
	 *            - The name of the field you want the set the value of
	 * @param itemValue
	 *            - The value to set the itemName field to
	 */
	public void setValue(Object itemName, Object itemValue) {
		XBlogUtils.getCurrentInstance().dbar.info("itemName = " + itemName.toString() + " itemValue = " + itemValue.toString(),
				"DataProvider.setValue(Object,Object)");
		Object oldValue = getValue(itemName);
		Boolean valueChanged = false;
		if ((oldValue != null && !itemValue.equals(oldValue)) || (itemValue != null && oldValue == null)) {
			valueChanged = true;
		}
		if (valueChanged) {
			try {
				if (itemValue == null || itemValue.equals("")) {
					changedValues.put(itemName.toString(), null);
				} else {
					if (itemValue instanceof DateTime) {
						DateTime nDate = (DateTime) itemValue;
						itemValue = nDate.toJavaDate();
					}
					changedValues.put(itemName.toString(), itemValue);
				}
			} catch (NotesException e) {
				e.printStackTrace();
			}
		}
	}

}
