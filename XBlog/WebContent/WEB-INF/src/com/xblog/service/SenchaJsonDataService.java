package com.xblog.service;

/*
 * This is the custom REST service for Sencha Touch integration. This class determines
 * what is being requested, makes a call to the output stream. The output stream makes
 * a call to the SenchaBlogParser which builds the JSON and returns it
 */

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lotus.domino.Database;
import lotus.domino.View;

import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.domino.services.rest.das.view.RestViewColumn;
import com.ibm.domino.services.rest.das.view.RestViewJsonService;
import com.ibm.domino.services.rest.das.view.ViewParameters;
import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.ibm.xsp.extlib.component.rest.DominoViewJsonService;
import com.ibm.xsp.extlib.component.rest.UIBaseRestService;

public class SenchaJsonDataService extends DominoViewJsonService {

	@Override
	public RestServiceEngine createEngine(FacesContext context, UIBaseRestService parent, HttpServletRequest httpRequest,
			HttpServletResponse httpResponse) {
		Parameters params = new Parameters(context, parent, httpRequest);
		return new Engine(httpRequest, httpResponse, params);
	}

	private class Engine extends RestViewJsonService {
		Engine(HttpServletRequest httpRequest, HttpServletResponse httpResponse, Parameters params) {
			super(httpRequest, httpResponse, params);
		}

		public View getView(String name) throws Exception {
			Database db;
			if (getDatabaseName() != null) {
				db = NotesContext.getCurrent().getCurrentSession().getDatabase(null, getDatabaseName());
			} else {
				db = NotesContext.getCurrent().getCurrentDatabase();
			}

			View dataView = db.getView(getViewName());
			return dataView;
		}

		@Override
		public void renderService() throws ServiceException {

			getHttpResponse().setContentType("application/json");
			getHttpResponse().setCharacterEncoding("utf-8");
			/*
			 * Figure out what we're requesting from the REST service
			 */
			String entryType = getHttpRequest().getParameter("type");
			String startEntry = getHttpRequest().getParameter("start");
			String entryLimit = getHttpRequest().getParameter("limit");
			
			int startRow = 0;
			if (startEntry != null) {
				startRow = Integer.parseInt(startEntry);
			}
			int rows = 0;
			if (entryLimit != null) {
				rows = Integer.parseInt(entryLimit);
			}

			try {
				SenchaDataOutputStream output = new SenchaDataOutputStream(getHttpResponse().getOutputStream());

				View dataView = getView(getViewName());
				String sortColumn = getHttpRequest().getParameter("sort");

				if ((entryType != null && entryType.equalsIgnoreCase("DOWNLOADS.json"))) {
					entryType = "DOWNLOADS";
					output.writeEntries(dataView, rows, entryType, null, startRow);
				} else if (((entryType != null) && entryType.equalsIgnoreCase("BLOG.json"))
						&& !getHttpRequest().getParameterMap().containsKey("searchValue")) {
					entryType = "BLOG";
					output.writeEntries(dataView, rows, entryType, null, startRow);
				} else if (((entryType != null) && entryType.equalsIgnoreCase("FULLARCHIVE.json"))) {
					entryType = "FULLARCHIVE";
					output.writeEntries(dataView, rows, entryType, null, startRow);
				} else if (((entryType != null) && entryType.equalsIgnoreCase("TOP.json"))) {
					entryType = "TOP";
					output.writeEntries(dataView, rows, entryType, null, startRow);
				} else if (((entryType != null) && (entryType.equalsIgnoreCase("CONTENT.json") || entryType.equalsIgnoreCase("CONTENT")))) {
					entryType = "CONTENT";
					String unid = getHttpRequest().getParameter("unid");
					if (unid.indexOf(".json") > -1) {
						unid = unid.substring(0, unid.indexOf(".json"));
					}
					output.writeContent(entryType, unid);
				} else if ((entryType !=null) && (entryType.equalsIgnoreCase("COMMENTS.json") || entryType.equalsIgnoreCase("COMMENTS"))) {
					entryType = "COMMENTS";
					String parentUnid = getHttpRequest().getParameter("parentUnid");
					if (parentUnid.indexOf(".json") > -1) {
						parentUnid = parentUnid.substring(0, parentUnid.indexOf(".json"));
					}
					output.writeEntries(dataView, rows, entryType, parentUnid, 0);
				} else {
					//System.out.println("com.xblog.service.SenchaJsonDataService - Don't know the type :(" + entryType + ") being requested");
				}
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected class Parameters implements ViewParameters {
		Parameters(FacesContext context, UIBaseRestService parent, HttpServletRequest httpRequest) {
		}

		public boolean isIgnoreRequestParams() {
			return SenchaJsonDataService.this.isIgnoreRequestParams();
		}

		public int getStart() {
			return SenchaJsonDataService.this.getStart();
		}

		public int getCount() {
			return SenchaJsonDataService.this.getCount();
		}

		public boolean isCompact() {
			return SenchaJsonDataService.this.isCompact();
		}

		public String getContentType() {
			return SenchaJsonDataService.this.getContentType();
		}

		public String getCategoryFilter() {
			return SenchaJsonDataService.this.getCategoryFilter();
		}

		public List<RestViewColumn> getColumns() {
			return SenchaJsonDataService.this.getColumns();
		}

		public String getDatabaseName() {
			return SenchaJsonDataService.this.getDatabaseName();
		}

		public String getSearch() {
			return SenchaJsonDataService.this.getSearch();
		}

		public int getGlobalValues() {
			return SenchaJsonDataService.this.getGlobalValues();
		}

		public int getExpandLevel() {
			return SenchaJsonDataService.this.getExpandLevel();
		}

		public String getSortColumn() {
			return SenchaJsonDataService.this.getSortColumn();
		}

		public int getSystemColumns() {
			return SenchaJsonDataService.this.getSystemColumns();
		}

		public String getVar() {
			return SenchaJsonDataService.this.getVar();
		}

		public String getViewName() {
			return SenchaJsonDataService.this.getViewName();
		}

		public boolean isDefaultColumns() {
			return SenchaJsonDataService.this.isDefaultColumns();
		}

		public String getSortOrder() {
			return SenchaJsonDataService.this.getSortOrder();
		}

		public int getSearchMaxDocs() {
			return SenchaJsonDataService.this.getSearchMaxDocs();
		}

		public Object getKeys() {
			return SenchaJsonDataService.this.getKeys();
		}

		public String getParentId() {
			return SenchaJsonDataService.this.getParentId();
		}

		public boolean isKeysExactMatch() {
			return SenchaJsonDataService.this.isKeysExactMatch();
		}

		public String getFormName() {
			return SenchaJsonDataService.this.getFormName();
		}

		public boolean isComputeWithForm() {
			return SenchaJsonDataService.this.isComputeWithForm();
		}

		public String getStartKeys() {
			return SenchaJsonDataService.this.getStartKeys().toString();
		}

		public boolean isEntryCount() {
			return false;
		}
	}

}
