package com.xblog.service;

/*
 * This is the output stream read by the REST service
 */

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;

import lotus.domino.View;

import com.xblog.service.data.SenchaBlogParser;

public class SenchaDataOutputStream extends BufferedOutputStream {

	public SenchaDataOutputStream(OutputStream arg0) {
		super(arg0);
	}

	/**
	 * Write view entries
	 * @param dataView lotus.domino.View - The view to get entries from
	 * @param rows java.lang.Integer - The number of rows to return
	 * @param scope java.lang.String - The type of content we're looking for. Valid values are DATA, DOWNLOADS, FULLARCHIVE, CONTENT, TOP
	 * @param archiveKey java.lang.String - Not yet used but will be for a particular month's archive
	 * @param startEntry java.lang.Integer - The starting viewEntry - Not yet used
	 */
	public void writeEntries(View dataView, int rows, String scope, String archiveKey, int startEntry) {
		try {
			StringWriter sw = new StringWriter();
			SenchaBlogParser parser = new SenchaBlogParser();
			if (archiveKey != null && scope.equalsIgnoreCase("fullarchive")) {
				parser.writeEntries(sw, scope, rows, archiveKey, startEntry);
			} else if (archiveKey != null && scope.equalsIgnoreCase("comments")) {
				parser.writeEntries(sw, scope, rows, archiveKey);
			} else {
				parser.writeEntries(sw, scope, rows, null, startEntry);
			}
			write(sw.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write blog post/page content
	 * @param scope java.lang.String - The type of content we're looking for. Valid values are DATA, DOWNLOADS, FULLARCHIVE, CONTENT, TOP
	 * @param unid java.lang.String - The unid of the document we want content from. This will not return view data but a post's/page's content
	 */
	public void writeContent(String scope, String unid) {
		try {
			StringWriter sw = new StringWriter();
			SenchaBlogParser parser = new SenchaBlogParser();
			parser.writeContent(sw, unid);
			write(sw.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
