package com.xblog.service.data;

/*
 * This class actually writes the JSON which is requested from the
 * SenchaDataOutputStream which is then sent back to the REST service
 */

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.Item;
import lotus.domino.MIMEEntity;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

import com.ibm.domino.services.util.JsonWriter;
import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.frontend.DateFormatter;
import com.xblog.renderkit.utils.RenderUtils;
import com.xblog.utils.XBlogUtils;

public class SenchaBlogParser {

	private String scope;
	private int startEntry;
	
	public SenchaBlogParser() {

	}

	/**
	 * This is the entry point from SenchaDataOutputStream
	 * @param sw StringWriter - A string writer
	 * @param scope String - The scope of the request (i.e. "CONTENT", "DOWNLOADS", "BLOG", "FULLARCHIVE", "TOP", "COMMENTS")
	 * @param rows int - The number of rows to return
	 * @param archiveKey String - The category key to limit entries to
	 * @param startEntry int - The start entry (i.e. 50 would start at the 50th entry)
	 * @return StringWriter - The string writer containing the JSON output
	 */
	public StringWriter writeEntries(StringWriter sw, String scope, int rows, String archiveKey, int startEntry) {
		setStartEntry(startEntry);
		return writeEntries(sw, scope, rows, archiveKey);
	}
	
	/**
	 * Write only view entries
	 * @param sw StringWriter - the StringWriter from the OutputStream
	 * @param scope - String - The scope of the request, ie what we're requesting
	 * @param rows - Integer - The number of entries we're requesting
	 * @param archiveKey - String - If we're requesting a month's archives, we need
	 * 		a lookup key
	 * @return StringWriter - The JSON representing the data requested
	 */
	public StringWriter writeEntries(StringWriter sw, String scope, int rows, String archiveKey) {
		try {
			JsonWriter writer = new JsonWriter(sw, false);
			writer.startArray();
			this.scope = scope;
			if (scope.equalsIgnoreCase("DOWNLOADS")) {
				getDownloadsViewNav(writer, rows, archiveKey);
			} else if (scope.equalsIgnoreCase("BLOG")) {
				getBlogViewNav(writer, rows);
			} else if (scope.equalsIgnoreCase("FULLARCHIVE")) {
				getFullArchiveViewNav(writer, rows);
			} else if (scope.equalsIgnoreCase("TOP")) {
				getTopTenViewNav(writer, rows);
			} else if (scope.equalsIgnoreCase("COMMENTS")) {
				getCommentsViewNav(writer, archiveKey, scope);
			}
			writer.endArray();
			return sw;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Write only the content of a post
	 * @param sw StringWriter
	 * @param unid java.lang.String - The unid of the post we're writing
	 * @return StringWriter
	 */
	public StringWriter writeContent(StringWriter sw, String unid) {
		try {
			this.scope = "CONTENT";
			JsonWriter writer = new JsonWriter(sw, false);
			writer.startArray();
			writeJsonContent(writer, unid);
			writer.endArray();
			return sw;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void getCommentsViewNav(JsonWriter writer, String parentUnid, String scope) {
		try {
			this.scope = scope;
			View commentsView = NotesContext.getCurrent().getCurrentDatabase().getView("(luComments)");
			ViewEntryCollection vec = commentsView.getAllEntriesByKey(parentUnid);
			int rows = vec.getCount();
			if (rows > 0) {
				writeJsonEntries(writer, vec.getCount(), vec, scope);
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the JSON for a Full Archive request
	 * @param writer - JsonWriter
	 * @param rows - The number of records to return
	 */
	public void getFullArchiveViewNav(JsonWriter writer, int rows) {
		try {
			View archiveView = NotesContext.getCurrent().getCurrentDatabase().getView("(MobileFullArchive)");
			ViewEntryCollection vec = archiveView.getAllEntries();
			if (rows == 0) {
				rows = vec.getCount();
			}
			writeJsonEntries(writer, rows, vec, "fullarchive");
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the JSON for a downloads request
	 * @param writer - JsonWriter
	 * @param rows - Integer - The number of records to return
	 * @param archiveKey - String - The lookup key for a month/year
	 */
	public void getDownloadsViewNav(JsonWriter writer, int rows, String archiveKey) {
		try {
			View downloadsView = NotesContext.getCurrent().getCurrentDatabase().getView("(luDownloads)");
			ViewEntryCollection vec = downloadsView.getAllEntries();
			if (rows == 0) {
				rows = vec.getCount();
			}
			writeJsonEntries(writer, rows, vec, "downloads");
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the JSON for the front page blog
	 * @param writer - JsonWriter
	 * @param rows - The number of records to return
	 */
	public void getBlogViewNav(JsonWriter writer, int rows) {
		try {
			View blogView = NotesContext.getCurrent().getCurrentDatabase().getView("(homeBlogsByDate)");
			ViewEntryCollection vec = blogView.getAllEntries();
			if (rows == 0) {
				rows = vec.getCount();
			}
			writeJsonEntries(writer, rows, vec, "blog");
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the JSON for the Top 10 posts
	 * @param writer - JsonWriter
	 * @param rows - The number of records to return
	 */
	public void getTopTenViewNav(JsonWriter writer, int rows) {
		try {
			View topTenView = NotesContext.getCurrent().getCurrentDatabase().getView("(luPopularNum)");
			ViewEntryCollection vec = topTenView.getAllEntries();
			rows = 10;
			writeJsonEntries(writer, rows, vec, "blog");
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes a json property
	 * @param writer JsonWriter - The JsonWriter
	 * @param propertyName String - The name of the property we're writing
	 * @param propertyValue Object - The value we're writing
	 * @throws IOException
	 */
	private void writeJsonProperty(JsonWriter writer, String propertyName, Object propertyValue) throws IOException {
		writer.startProperty(propertyName);
		if (propertyValue instanceof String) {
			writer.outStringLiteral((String) propertyValue);
		}else if (propertyValue instanceof Boolean) {
			writer.outBooleanLiteral((Boolean) propertyValue);
		}else if (propertyValue instanceof Integer) {
			writer.outIntLiteral((Integer) propertyValue);
		}else if (propertyValue instanceof Long) {
			writer.outLongLiteral((Long) propertyValue);
		}else if (propertyValue instanceof Date) {
			writer.outDateLiteral((Date) propertyValue);
		}else if (propertyValue instanceof DateTime) {
			writer.outDateLiteral((DateTime) propertyValue);
		}
		writer.endProperty();
	}
	
	/**
	 * Write the actual JSON for all the different types of requests
	 * @param writer - JsonWriter
	 * @param rows - Integer - The number of records to return
	 * @param vec - ViewEntryCollection - The collection of records to return
	 * @param scope - String - What we want to return
	 */
	public void writeJsonEntries(JsonWriter writer, int rows, ViewEntryCollection vec, String scope) {
		try {
			int count = 1;
			ViewEntry ent;
			if (getStartEntry() != 0) {
				ent = vec.getNthEntry(getStartEntry() -1);
			}else{
				ent = vec.getFirstEntry();
			}
			while (ent != null && count <= rows) {
				Document doc = ent.getDocument();
				writer.startArrayItem();
				writer.startObject();
				if (!scope.equalsIgnoreCase("comments")) {
					writeJsonProperty(writer, "title", doc.getItemValueString("title"));
				}else{
					writeJsonProperty(writer, "title", ent.getColumnValues().get(5).toString());
				}
				writeJsonProperty(writer, "createdDate", getCreatedDate(doc));
				writeJsonProperty(writer, "unid", doc.getUniversalID());
				if (scope.equalsIgnoreCase("fullarchive")) {
					writeJsonProperty(writer, "category", ent.getColumnValues().get(1).toString());
				}else if (scope.equalsIgnoreCase("comments")) {
					writeJsonProperty(writer, "parentUnid", doc.getParentDocumentUNID());
					writeJsonProperty(writer, "author", ent.getColumnValues().get(2).toString());
					writeJsonProperty(writer, "website", ent.getColumnValues().get(4).toString());
					writeJsonProperty(writer, "content", doc.getItemValueString("Comment"));
				}
				writer.endObject();
				writer.endArrayItem();
				ViewEntry prevEnt = ent;
				ent = vec.getNextEntry(ent);
				prevEnt.recycle();
				count++;
			}
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the created date
	 * @param writer JsonWriter
	 * @param doc lotus.domino.Document - The document we're working with
	 */
	@Deprecated
	private void writeDate(JsonWriter writer, Document doc) {
		try {
			writer.startProperty("createdDate");
			DateTime nDate = NotesContext.getCurrent().getCurrentSession()
					.createDateTime(doc.getItemValue("CreatedDate").get(0).toString());
			String dateFormat = XBlogUtils.getCurrentInstance().getConfigValue("PostDateFormat", 0).toString();
			String formattedDate = DateFormatter.getFormattedDate(dateFormat, nDate.toJavaDate());
			writer.outStringLiteral(formattedDate);
			writer.endProperty();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}

	private String getCreatedDate(Document doc) {
		String returnDate = "";
		try {
			DateTime nDate = NotesContext.getCurrent().getCurrentSession()
					.createDateTime(doc.getItemValue("CreatedDate").get(0).toString());
			String dateFormat = XBlogUtils.getCurrentInstance().getConfigValue("PostDateFormat", 0).toString();
			String formattedDate = DateFormatter.getFormattedDate(dateFormat, nDate.toJavaDate());
			returnDate = formattedDate;
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return returnDate;
	}
	
	/**
	 * Convert MIME to a String (i.e. The HTML for a post/page content)
	 * @param doc Document the document we're processing
	 * @return String
	 */
	private String getMimeContent(Document doc, String itemName) {
		try {
			Item rtItem = doc.getFirstItem(itemName);
			if (rtItem != null) {
				MIMEEntity mime = rtItem.getMIMEEntity();
				return mime.getContentAsText();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void writeJsonContent(JsonWriter writer, String unid) {
		try {
			writer.startArrayItem();
			writer.startObject();
			if (unid != null && !unid.isEmpty()) {
				Database db = NotesContext.getCurrent().getCurrentDatabase();
				Document doc = db.getDocumentByUNID(unid);
				String blogContent = RenderUtils.replaceHotText(getMimeContent(doc, "postContent"), null);
				blogContent = blogContent + "<br><br>";
				if (getMimeContent(doc, "readMoreContent") != null) {
					blogContent = blogContent + RenderUtils.replaceHotText(getMimeContent(doc, "readMoreContent"), null);
				}
				writeJsonProperty(writer, "content", blogContent);
				writeJsonProperty(writer, "unid", doc.getUniversalID());
				writeJsonProperty(writer, "createdDate", getCreatedDate(doc));
				writeJsonProperty(writer, "title", doc.getItemValueString("title"));
				if (doc.hasEmbedded()) {
					writeJsonProperty(writer, "attachmentHtml", RenderUtils.buildAttachmentHtml(unid));
				}
				if (doc.getItemValueString("allowComments").equalsIgnoreCase("yes")) {
					writeJsonProperty(writer, "allowComments",false);
				}else{
					writeJsonProperty(writer, "allowComments", true);
				}
				writeJsonProperty(writer, "postCommentButtonText", RenderUtils.getTranslationText("<$TransPostComment$>"));
				writeJsonProperty(writer, "commentText", RenderUtils.getTranslationText("<$TransComments$>"));
				writeJsonProperty(writer, "commentSave", RenderUtils.getTranslationText("<$TransCommentSave$>"));
				writeJsonProperty(writer, "commentCancel",RenderUtils.getTranslationText("<TransCommentCancel$>"));
			} else {
				writeJsonProperty(writer, "unid", "<b>No UNID!</b>");
				writeJsonProperty(writer, "title", "No UNID");
			}
			writer.endObject();
			writer.endArrayItem();
		} catch (NotesException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getStartEntry() {
		return startEntry;
	}
	
	public void setStartEntry(int startEntry) {
		this.startEntry = startEntry;
	}

	@Deprecated
	private String getMimeReadMore(Document doc) {
		try {
			Item rtItem = doc.getFirstItem("readMoreContent");
			if (rtItem != null && rtItem.getMIMEEntity() != null) {
				MIMEEntity mime = rtItem.getMIMEEntity();
				return mime.getContentAsText();
			}
		} catch (NotesException e) {
			e.printStackTrace();
		}
		return "";
	}
}
