package com.xblog.interfaces;

public interface IPageAction {

	public Object execute(String args);
	
	public Object execute();
	
}
