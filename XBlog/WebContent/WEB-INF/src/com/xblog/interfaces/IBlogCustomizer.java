package com.xblog.interfaces;

/**
 * This interface is the defining factor in something similar to a customizer bean. It will allow you to
 * take over or add to the rendering of any XBlog specific components.
 * 
 * At the moment this is just a proof of concept and eventually will evolve into a plugin system to allow
 * you to create plugins to take over and/or add to the functionality of XBlog.
 */

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

public interface IBlogCustomizer {

	public Boolean isRenderParent();
	
	public Boolean setRenderParent();
	
	public void encodeBegin(FacesContext context, UIComponent component);
	
	public void encodeChildren(FacesContext context, UIComponent component);
	
	public void encodeEnd(FacesContext context, UIComponent component);
	
	public void beforeEncodeBegin();
	
	public void afterEncodeBegin();
	
	public void beforeEncodeEnd();
	
	public void afterEncodeEnd();
	
}
