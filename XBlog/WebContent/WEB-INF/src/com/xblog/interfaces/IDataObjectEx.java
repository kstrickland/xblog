package com.xblog.interfaces;

import com.ibm.xsp.model.DataObject;

public interface IDataObjectEx extends DataObject {

	public void save(String formName);
	
	public void save();
}
