package com.xblog.interfaces;

import lotus.domino.Document;

/**
 * This class represents a post, page or comment
 * @author Keith Strickland
 *
 */
public interface IEntry {

	public String getUnid();

	public String getCreatedDate();

	public Document getThisDoc();

	public String getAuthor();
}
