package com.xblog.components;

/*
 * This class is in preparation of a plug-in system if possible. If not able to 
 * implement a plug-in system, then classes are just a better way to manage all
 *  the different aspects of XBlog.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.servlet.http.HttpServletRequest;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.EmbeddedObject;
import lotus.domino.Item;
import lotus.domino.MIMEEntity;
import lotus.domino.NotesException;
import lotus.domino.RichTextItem;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.interfaces.IEntry;
import com.xblog.renderkit.utils.RenderUtils;
import com.xblog.utils.XBlogUtils;

public class Post extends UIComponentBase implements IEntry {

	private static String FAMILY = "com.xblog";
	private static String COMPONENT_TYPE = "com.xblog.Post";
	private static String RENDERER_TYPE = "com.xblog.Post";

	private List<String> attachments;
	private String author;
	private boolean commentsDisabled;
	private String commentsPermalink;
	private String createdDate;
	private String department;
	private String friendlyUrl;
	private String location;
	private String mimeContent;
	private String mimeReadMore;
	private int numberComments;
	private String permalink;
	private boolean post;
	private Date publishDate;
	private boolean published;
	private String tags;
	private String textReadMore;
	private String textContent;
	private String title;
	private String unid;
	private int viewed;
	private Document thisDoc;
	private String defaultHtml;

	public Post() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		setRendererType(RENDERER_TYPE);
	}

	@Override
	public void encodeBegin(FacesContext context) throws IOException {
		try {
			if (unid == null) {
				String thisUnid = getUnid();
			}
			/*
			 * OK, we're setting the attachments here because we don't
			 * have a backing bean that we can use to determine the attachments in a
			 * more elegant way. This makes it easy to determine if any attachments
			 * exist and if they do, then set the property appropriately
			 */
			Document doc = getThisDoc();
			if (doc != null && !isPost()) {
				RichTextItem rtItem = (RichTextItem) doc.getFirstItem("attachments");
				if (rtItem != null) {
					Vector<EmbeddedObject> eoList = rtItem.getEmbeddedObjects();
					if (eoList.size() > 0) {
						List<String> attachList = new ArrayList<String>();
						String attachPath = XBlogUtils.getCurrentInstance().getBlogUrl() + "/0/" + getUnid() + "/$FILE/";
						for (EmbeddedObject eo : eoList) {
							attachList.add(attachPath + eo.getName());
						}
						setAttachments(attachList);
					}
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.encodeBegin(FacesContext)");
			e.printStackTrace();
		}
		super.encodeBegin(context);
	}

	public String getAuthor() {
		try {
			if (author != null) {
				return author;
			}
			ValueBinding vb = getValueBinding("author");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (author == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("author");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getAuthor()");
			e.printStackTrace();
			return null;
		}
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public List<String> getAttachments() {
		try {
			if (attachments != null) {
				return attachments;
			}
			ValueBinding vb = getValueBinding("attachments");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (List) vb.getValue(FacesContext.getCurrentInstance());
			} else if (attachments == null && getThisDoc() != null) {
				List<String> attList = new ArrayList<String>();
				String blogUrl = XBlogUtils.getCurrentInstance().getBlogUrl();
				RichTextItem rtItem = (RichTextItem) getThisDoc().getFirstItem("attachments");
				if (rtItem != null) {
					Vector<EmbeddedObject> eoList = rtItem.getEmbeddedObjects();
					if (eoList.size() > 0) {
						for (EmbeddedObject eo : eoList) {
							attList.add(blogUrl + "/0/" + getUnid() + "/$FILE/" + eo.getName());
						}
						return attList;
					}
				}
				return null;
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getAttachments()");
			e.printStackTrace();
			return null;
		}
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

	/*
	 * DOH! got this boolean flipped :( so allowComments = Yes is really No
	 */
	public boolean isCommentsDisabled() {
		try {
			ValueBinding vb = getValueBinding("commentsDisabled");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return !(Boolean) vb.getValue(FacesContext.getCurrentInstance());
			} else if (getThisDoc() == null) {
				return false;
			} else {
				if (getThisDoc().getItemValueString("allowComments").equalsIgnoreCase("Yes")) {
					return false;
				} else {
					return true;
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.isCommentsDisabled()");
			e.printStackTrace();
			return false;
		}
	}

	public void setCommentsDisabled(boolean commentsDisabled) {
		this.commentsDisabled = commentsDisabled;
	}

	public String getCommentsPermalink() {
		try {
			if (commentsPermalink != null) {
				return commentsPermalink;
			}
			ValueBinding vb = getValueBinding("commentsPermalink");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (commentsPermalink == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("commentsPermalink");
			} else {
				if (getPermalink() != null) {
					return getPermalink() + "#comments";
				} else {
					return null;
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getCommentsPermalink()");
			e.printStackTrace();
			return null;
		}
	}

	public void setCommentsPermalink(String commentsPermalink) {
		this.commentsPermalink = commentsPermalink;
	}

	public String getCreatedDate() {
		try {
			if (createdDate != null) {
				return createdDate;
			}
			ValueBinding vb = getValueBinding("createdDate");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (createdDate == null && getThisDoc() != null) {
				String dtStr = getThisDoc().getItemValue("createdDate").get(0).toString();
				DateTime nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(dtStr);
				return nDate.toString();
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getCreatedDate()");
			e.printStackTrace();
			return null;
		}
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getDepartment() {
		try {
			if (department != null) {
				return department;
			}
			ValueBinding vb = getValueBinding("department");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (department == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("department");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getDepartment()");
			e.printStackTrace();
			return null;
		}
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFriendlyUrl() {
		try {
			if (friendlyUrl != null) {
				return friendlyUrl;
			}
			ValueBinding vb = getValueBinding("friendlyUrl");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (friendlyUrl == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("OldPermalink");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getFriendlyUrl()");
			e.printStackTrace();
			return null;
		}
	}

	public void setFriendlyUrl(String friendlyUrl) {
		this.friendlyUrl = friendlyUrl;
	}

	public String getLocation() {
		try {
			if (location != null) {
				return location;
			}
			ValueBinding vb = getValueBinding("location");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (location == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("location");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getLocation()");
			e.printStackTrace();
			return null;
		}
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMimeContent() {
		try {
			if (mimeContent != null) {
				return mimeContent;
			}
			ValueBinding vb = getValueBinding("mimeContent");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return RenderUtils.replaceHotText((String) vb.getValue(FacesContext.getCurrentInstance()), this);
			} else if (mimeContent == null && getThisDoc() != null) {
				Item rtContent = getThisDoc().getFirstItem("postContent");
				MIMEEntity mimeContent = rtContent.getMIMEEntity();
				return RenderUtils.replaceHotText(mimeContent.getContentAsText(), this);
			} else {
				XBlogUtils.getCurrentInstance().dbar.warn("Post MIME Content is null", "Post.getMimeContent()");
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getMimeContent()");
			e.printStackTrace();
			return null;
		}
	}

	public void setMimeContent(String mimeContent) {
		this.mimeContent = mimeContent;
	}

	public String getMimeReadMore() {
		try {
			if (mimeReadMore != null) {
				return mimeReadMore;
			}
			ValueBinding vb = getValueBinding("mimeReadMore");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return RenderUtils.replaceHotText((String) vb.getValue(FacesContext.getCurrentInstance()), this);
			} else if (mimeReadMore == null && getThisDoc() instanceof Document) {
				Item rtContent = getThisDoc().getFirstItem("readMoreContent");
				if (rtContent != null) {
					MIMEEntity mimeContent = rtContent.getMIMEEntity();
					return RenderUtils.replaceHotText(mimeContent.getContentAsText(), this);
				} else {
					//XBlogUtils.getCurrentInstance().dbar.warn("Post MIME Readmore is null", "Post.getMimeReadMore()");
					return null;
				}
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getMimeReadMore()");
			e.printStackTrace();
			return null;
		}
	}

	public void setMimeReadMore(String mimeReadMore) {
		this.mimeReadMore = mimeReadMore;
	}

	public int getNumberComments() {
		try {
			if (numberComments != 0) {
				return numberComments;
			}
			ValueBinding vb = getValueBinding("numberComments");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Integer) vb.getValue(FacesContext.getCurrentInstance());
			} else {
				return XBlogUtils.getCurrentInstance().getNumberComments(getUnid());
			}
		} catch (Exception e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getNumberComments()");
			e.printStackTrace();
			return 0;
		}
	}

	public void setNumberComments(int numberComments) {
		this.numberComments = numberComments;
	}

	public String getPermalink() {
		try {
			if (permalink != null) {
				return permalink;
			}
			ValueBinding vb = getValueBinding("permalink");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (permalink == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("permalink");
			} else {
				return "no permalink";
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getPermalink()");
			e.printStackTrace();
			return null;
		}
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public boolean isPost() {
		return post;
	}

	public void setPost(boolean post) {
		this.post = post;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public boolean isPublished() {
		try {
			ValueBinding vb = getValueBinding("published");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Boolean) vb.getValue(FacesContext.getCurrentInstance());
			} else if (getThisDoc() != null) {
				if (getThisDoc().getItemValueString("publish").equalsIgnoreCase("Yes")) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.isPublished()");
			e.printStackTrace();
			return false;
		}
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public String getTags() {
		try {
			if (tags != null) {
				return tags;
			}
			ValueBinding vb = getValueBinding("tags");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (tags == null && getThisDoc() != null) {
				return XBlogUtils.getCurrentInstance().vectorToString(getThisDoc().getItemValue("tags"));
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getTags()");
			e.printStackTrace();
			return null;
		}
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getTextReadMore() {
		return textReadMore;
	}

	public void setTextReadMore(String textReadMore) {
		this.textReadMore = textReadMore;
	}

	public String getTextContent() {
		return textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public String getTitle() {
		try {
			if (title != null) {
				return title;
			}
			ValueBinding vb = getValueBinding("title");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (title == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("title");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getTitle()");
			e.printStackTrace();
			return null;
		}
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUnid() {
		try {
			if (unid != null) {
				return unid;
			}
			ValueBinding vb = getValueBinding("unid");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else {
				XBlogUtils.getCurrentInstance().dbar.warn("Post UNID is null", "Post.getUnid()");
				return null;
			}
		} catch (Exception e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getUnid()");
			e.printStackTrace();
			return null;
		}
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public int getViewed() {
		try {
			ValueBinding vb = getValueBinding("viewed");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Integer) vb.getValue(FacesContext.getCurrentInstance());
			} else if (viewed == 0 && getThisDoc() != null) {
				return getThisDoc().getItemValueInteger("viewNumber");
			} else {
				return 0;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getViewed()");
			e.printStackTrace();
			return 0;
		}
	}

	public void setViewed(int viewed) {
		this.viewed = viewed;
	}

	public Document getThisDoc() {
		try {
			if (thisDoc != null) {
				thisDoc = null;
			}
			Database db = NotesContext.getCurrent().getCurrentDatabase();
			ValueBinding vb = getValueBinding("thisDoc");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Document) vb.getValue(FacesContext.getCurrentInstance());
			} else if (thisDoc == null && unid != null) {
				thisDoc = db.getDocumentByUNID(unid);
				return thisDoc;
			} else {
				if (unid == null) {
					String thisUnid = getUnid();
					if (thisUnid != null) {
						return db.getDocumentByUNID(thisUnid);
					} else {
						return null;
					}
				}
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Post.getThisDoc()");
			e.printStackTrace();
			return null;
		}
	}

	public void setThisDoc(Document thisDoc) {
		try {
			if (this.getThisDoc() != null) {
				this.thisDoc.recycle();
			}
		} catch (NotesException e) {
			// Do Nothing
		}
		this.thisDoc = thisDoc;
	}

	public String getDefaultHtml() {
		XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
		if (defaultHtml != null) {
			return defaultHtml;
		}
		ValueBinding vb = getValueBinding("defaultHtml");
		if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
			return (String) vb.getValue(FacesContext.getCurrentInstance());
		} else if (isPost()) {
			Object defaultHtml = xbUtils.getLayoutValue("EntryLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		} else if (!isPost()) {
			Object defaultHtml = xbUtils.getLayoutValue("PageLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		} else {
			XBlogUtils.getCurrentInstance().dbar.error("No Layout Information Available", "Post.getDefaultHtml()");
			return "<h2>No Layout HTML is available. Please build your Layout in the Layout form, Entry Defaults tab!</h2>";
		}
	}

	public void setDefaultHtml(String defaultHtml) {
		this.defaultHtml = defaultHtml;
	}

	@Override
	public String getFamily() {
		return FAMILY;
	}

	@Override
	public Object saveState(FacesContext context) {
		Object values[] = new Object[19];
		values[0] = super.saveState(context);
		values[1] = unid;
		values[2] = title;
		values[3] = author;
		values[4] = publishDate;
		values[5] = location;
		values[6] = department;
		values[7] = friendlyUrl;
		values[8] = tags;
		values[9] = mimeContent;
		values[10] = mimeReadMore;
		values[11] = numberComments;
		values[12] = viewed;
		values[13] = attachments;
		values[14] = published;
		values[15] = post;
		values[16] = permalink;
		values[17] = commentsDisabled;
		values[18] = defaultHtml;
		return values;
	}

	@Override
	public void restoreState(FacesContext context, Object state) {
		Object values[] = (Object[]) state;
		super.restoreState(context, values[0]);
		unid = (String) values[1];
		title = (String) values[2];
		author = (String) values[3];
		publishDate = (Date) values[4];
		location = (String) values[5];
		department = (String) values[6];
		friendlyUrl = (String) values[7];
		tags = (String) values[8];
		mimeContent = (String) values[9];
		mimeReadMore = (String) values[10];
		numberComments = (Integer) values[11];
		viewed = (Integer) values[12];
		attachments = (List) values[13];
		published = (Boolean) values[14];
		post = (Boolean) values[15];
		permalink = (String) values[16];
		commentsDisabled = (Boolean) values[17];
		defaultHtml = (String) values[18];
	}
}
