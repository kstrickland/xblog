package com.xblog.components;

import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

import com.xblog.utils.XBlogUtils;

public class Archive extends Post {

	private static String FAMILY = "com.xblog";
	private static String COMPONENT_TYPE = "com.xblog.Archive";
	private static String RENDERER_TYPE = "com.xblog.Archive";

	private int characterCount = 1000;
	private String mimeContent;
	private String defaultHtml;

	public Archive() {
		setRendererType(RENDERER_TYPE);
	}

	@Override
	public String getDefaultHtml() {
		XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
		if (defaultHtml != null) {
			return defaultHtml;
		}
		ValueBinding vb = getValueBinding("defaultHtml");
		if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
			return (String) vb.getValue(FacesContext.getCurrentInstance());
		} else if (isPost()) {
			Object defaultHtml = xbUtils.getLayoutValue("ArchiveLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		} else if (!isPost()) {
			Object defaultHtml = xbUtils.getLayoutValue("PageLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		} else {
			return "<h2>No HTML found in the defined Layout Document. Please build your Layout in the Layout form, Entry Defaults tab!</h2>";
		}
	}

	@Override
	public String getFamily() {
		return FAMILY;
	}

	@Override
	public String getMimeContent() {
		String fullContent = super.getMimeContent();
		StringBuilder sb = new StringBuilder();		
		if (fullContent != null) {
			if (fullContent.length() < characterCount) {
				setCharacterCount(fullContent.length() - 10);
			}
			sb.append(fullContent.substring(0, characterCount));
			sb.append("<b><a href='" + XBlogUtils.getCurrentInstance().getBlogUrl() + "/default.xsp?documentId=" + getUnid() + "'>");
			sb.append("<$TransOpenArchive$></a></b>");
		}
		return sb.toString();
	}

	public int getCharacterCount() {
		try {
			if (characterCount != 1000) {
				return characterCount;
			}
			ValueBinding vb = getValueBinding("characterCount");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Integer) vb.getValue(FacesContext.getCurrentInstance());
			} else {
				return 1000;
			}
		} catch (Exception e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Archive.getCharacterCount()");
			e.printStackTrace();
			return 1000;
		}
	}

	public void setCharacterCount(int characterCount) {
		this.characterCount = characterCount;
	}

	@Override
	public Object saveState(FacesContext context) {
		Object values[] = new Object[4];
		int idx = 0;
		values[idx] = super.saveState(context);
		values[idx++] = this.characterCount;
		values[idx++] = this.defaultHtml;
		values[idx++] = this.mimeContent;
		return values;
	}

	@Override
	public void restoreState(FacesContext context, Object state) {
		Object values[] = (Object[]) state;
		int idx = 0;
		super.restoreState(context, values[idx]);
		this.characterCount = (Integer) values[idx++];
		this.defaultHtml = (String) values[idx++];
		this.mimeContent = (String) values[idx++];
	}

}
