package com.xblog.components;

import java.util.Vector;

import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.Item;
import lotus.domino.MIMEEntity;
import lotus.domino.NotesException;

import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.xblog.interfaces.IEntry;
import com.xblog.renderkit.utils.MD5Util;
import com.xblog.utils.XBlogUtils;

/**
 * A comment for a blog post. Uses renderer com.xblog.renderkit.CommentRenderer
 * @author Keith Strickland
 *
 */
public class Comment extends UIComponentBase implements IEntry {

	private static String FAMILY = "com.xblog";
	private static String COMPONENT_TYPE = "com.xblog.Comment";
	private static String RENDERER_TYPE = "com.xblog.Comment";

	private String author;
	private boolean blocked;
	private int commentNumber;
	private String commentText;
	private String defaultHtml;
	private String department;
	private String emailAddress;
	private String gravatar;
	private String ipAddress;
	private boolean notifyUser;
	private String parentUnid;
	private boolean pendingApproval;
	private String createdDate;
	private String referrer;
	private String subject;
	private String unid;
	private String userAgent;
	private String webSite;
	private Document thisDoc;

	public Comment() {
		setRendererType(RENDERER_TYPE);
	}

	public String getAuthor() {
		try {
			if (author != null) {
				return author;
			}
			ValueBinding vb = getValueBinding("author");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (author == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("name");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getAuthor()");
			e.printStackTrace();
			return null;
		}
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isBlocked() {
		try {
			ValueBinding vb = getValueBinding("blocked");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return !(Boolean) vb.getValue(FacesContext.getCurrentInstance());
			} else if (getThisDoc() == null) {
				return false;
			} else {
				if (getThisDoc().getItemValueString("blocked").equalsIgnoreCase("Yes")) {
					return true;
				} else {
					return false;
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.isBlocked()");
			e.printStackTrace();
			return false;
		}
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public int getCommentNumber() {
		try {
			ValueBinding vb = getValueBinding("commentNumber");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Integer) vb.getValue(FacesContext.getCurrentInstance());
			} else if (commentNumber == 0 && getThisDoc() != null) {
				return getThisDoc().getItemValueInteger("CommentNumber");
			} else if (commentNumber != 0) {
				return commentNumber;
			} else {
				return 0;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getCommentNumber()");
			e.printStackTrace();
			return 0;
		}
	}

	public void setCommentNumber(int commentNumber) {
		this.commentNumber = commentNumber;
	}

	public String getCommentText() {
		try {
			if (commentText != null) {
				return commentText;
			}
			ValueBinding vb = getValueBinding("commentText");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (commentText == null && getThisDoc() != null) {
				Item rtContent = getThisDoc().getFirstItem("Comment");
				MIMEEntity mimeContent = rtContent.getMIMEEntity();
				return mimeContent.getContentAsText();
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getCommentText()");
			e.printStackTrace();
			return null;
		}
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getDefaultHtml() {
		XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
		if (defaultHtml != null) {
			return defaultHtml;
		}
		ValueBinding vb = getValueBinding("defaultHtml");
		if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
			return (String) vb.getValue(FacesContext.getCurrentInstance());
		} else {
			Object defaultHtml = xbUtils.getLayoutValue("CommentLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		}
	}

	public void setDefaultHtml(String defaultHtml) {
		this.defaultHtml = defaultHtml;
	}

	public String getDepartment() {
		try {
			if (department != null) {
				return department;
			}
			ValueBinding vb = getValueBinding("department");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (department == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("Department");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getDepartment()");
			e.printStackTrace();
			return null;
		}
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmailAddress() {
		try {
			if (emailAddress != null) {
				return emailAddress;
			}
			ValueBinding vb = getValueBinding("emailAddress");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (emailAddress == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("Email");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getEmailAddress()");
			e.printStackTrace();
			return null;
		}
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getGravatar() {
		try {
			if (gravatar != null) {
				return gravatar;
			}
			ValueBinding vb = getValueBinding("gravatar");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (gravatar == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("Gravatar");
			} else {
				String url = "http://www.gravatar.com/avatar/";
				String email = getEmailAddress().trim();
				if (email != null) {
					String hash = MD5Util.md5Hex(email);
					url = url + hash + ".png?r=g";
					String gravatarImg = "<img src='" + url + "'>";
					return gravatarImg;
				} else {
					return null;
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getGravatar()");
			e.printStackTrace();
			return null;
		}
	}

	public void setGravatar(String gravatar) {
		this.gravatar = gravatar;
	}

	public String getIpAddress() {
		try {
			if (ipAddress != null) {
				return ipAddress;
			}
			ValueBinding vb = getValueBinding("ipAddress");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (ipAddress == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("IPAddress");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getIpAddress()");
			e.printStackTrace();
			return null;
		}
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public boolean isNotifyUser() {
		try {
			ValueBinding vb = getValueBinding("notifyUser");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return !(Boolean) vb.getValue(FacesContext.getCurrentInstance());
			} else if (getThisDoc() == null) {
				return false;
			} else {
				if (getThisDoc().getItemValueString("notifyUser").equalsIgnoreCase("Yes")) {
					return true;
				} else {
					return false;
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.isNotifyUser()");
			e.printStackTrace();
			return false;
		}
	}

	public void setNotifyUser(boolean notifyUser) {
		this.notifyUser = notifyUser;
	}

	public String getParentUnid() {
		try {
			if (parentUnid != null) {
				return parentUnid;
			}
			ValueBinding vb = getValueBinding("parentUnid");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (parentUnid == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("parentUnid");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getParentUnid()");
			e.printStackTrace();
			return null;
		}
	}

	public void setParentUnid(String parentUnid) {
		this.parentUnid = parentUnid;
	}

	public boolean isPendingApproval() {
		try {
			ValueBinding vb = getValueBinding("pendingApproval");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return !(Boolean) vb.getValue(FacesContext.getCurrentInstance());
			} else if (getThisDoc() == null) {
				return false;
			} else {
				if (getThisDoc().getItemValueString("pendingApproval").equalsIgnoreCase("Yes")) {
					return true;
				} else {
					return false;
				}
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.isPendingApproval()");
			e.printStackTrace();
			return false;
		}
	}

	public void setPendingApproval(boolean pendingApproval) {
		this.pendingApproval = pendingApproval;
	}

	public String getCreatedDate() {
		try {
			if (createdDate != null) {
				return createdDate;
			}
			ValueBinding vb = getValueBinding("createdDate");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (createdDate == null && getThisDoc() != null) {
				String dtStr = getThisDoc().getItemValue("CreatedDate").get(0).toString();
				DateTime nDate = NotesContext.getCurrent().getCurrentSession().createDateTime(dtStr);
				return nDate.toString();
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getCreatedDate()");
			e.printStackTrace();
			return null;
		}
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getReferrer() {
		try {
			if (referrer != null) {
				return referrer;
			}
			ValueBinding vb = getValueBinding("referrer");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (referrer == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("Referrer");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getReferrer()");
			e.printStackTrace();
			return null;
		}
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public String getSubject() {
		try {
			if (subject != null) {
				return subject;
			}
			ValueBinding vb = getValueBinding("subject");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (subject == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("Subject");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getSubject()");
			e.printStackTrace();
			return null;
		}
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getUnid() {
		try {
			if (unid != null) {
				return unid;
			}
			ValueBinding vb = getValueBinding("unid");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (unid == null && getThisDoc() != null) {
				return getThisDoc().getUniversalID();
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getUnid()");
			e.printStackTrace();
			return null;
		}
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getUserAgent() {
		try {
			if (userAgent != null) {
				return userAgent;
			}
			ValueBinding vb = getValueBinding("userAgent");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (userAgent == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("userAgent");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getUserAgent()");
			e.printStackTrace();
			return null;
		}
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getWebSite() {
		try {
			if (webSite != null) {
				return webSite;
			}
			ValueBinding vb = getValueBinding("webSite");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (String) vb.getValue(FacesContext.getCurrentInstance());
			} else if (webSite == null && getThisDoc() != null) {
				return getThisDoc().getItemValueString("Website");
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getWebSite()");
			e.printStackTrace();
			return null;
		}
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public Document getThisDoc() {
		try {
			if (thisDoc != null) {
				thisDoc = null;
			}
			ValueBinding vb = getValueBinding("thisDoc");
			if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
				return (Document) vb.getValue(FacesContext.getCurrentInstance());
			} else if (thisDoc == null && unid != null) {
				Database db = NotesContext.getCurrent().getCurrentDatabase();
				thisDoc = db.getDocumentByUNID(unid);
				return thisDoc;
			} else {
				return null;
			}
		} catch (NotesException e) {
			XBlogUtils.getCurrentInstance().dbar.error(e.getMessage(), "Comment.getThisDoc()");
			e.printStackTrace();
			return null;
		}
	}

	public void setThisDoc(Document thisDoc) {
		this.thisDoc = thisDoc;
	}

	@Override
	public String getFamily() {
		return FAMILY;
	}

	@Override
	public Object saveState(FacesContext context) {
		Object values[] = new Object[18];
		values[0] = super.saveState(context);
		values[1] = author;
		values[2] = blocked;
		values[3] = commentNumber;
		values[4] = commentText;
		values[5] = createdDate;
		values[6] = defaultHtml;
		values[7] = department;
		values[8] = emailAddress;
		values[9] = ipAddress;
		values[10] = notifyUser;
		values[11] = parentUnid;
		values[12] = pendingApproval;
		values[13] = referrer;
		values[14] = subject;
		values[15] = unid;
		values[16] = userAgent;
		values[17] = webSite;
		return values;
	}

	@Override
	public void restoreState(FacesContext context, Object state) {
		Object values[] = (Object[]) state;
		super.restoreState(context, values[0]);
		author = (String) values[1];
		blocked = (Boolean) values[2];
		commentNumber = (Integer) values[3];
		commentText = (String) values[4];
		createdDate = (String) values[5];
		defaultHtml = (String) values[6];
		department = (String) values[7];
		emailAddress = (String) values[8];
		ipAddress = (String) values[9];
		notifyUser = (Boolean) values[10];
		parentUnid = (String) values[11];
		pendingApproval = (Boolean) values[12];
		referrer = (String) values[13];
		subject = (String) values[14];
		unid = (String) values[15];
		userAgent = (String) values[16];
		webSite = (String) values[17];
	}
}