package com.xblog.components;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import com.ibm.xsp.component.UIPanelEx;
import com.ibm.xsp.component.UISelectItemEx;
import com.ibm.xsp.component.xp.XspOutputLink;
import com.ibm.xsp.component.xp.XspOutputText;
import com.ibm.xsp.component.xp.XspSelectOneMenu;
import com.ibm.xsp.component.xp.XspTable;
import com.ibm.xsp.component.xp.XspTableCell;
import com.ibm.xsp.component.xp.XspTableRow;
import com.ibm.xsp.dojo.DojoAttribute;
import com.ibm.xsp.extlib.component.containers.UIList;
import com.ibm.xsp.extlib.component.tagcloud.UITagCloud;
import com.ibm.xsp.extlib.component.tagcloud.ViewTagCloudData;
import com.xblog.renderkit.utils.RenderUtils;
import com.xblog.utils.JSFUtil;
import com.xblog.utils.XBlogUtils;

/**
 * An individual sidebar item. The renderer is com.xblog.renderkit.SideBarItemRenderer
 * @author Keith
 *
 */
public class SideBarItem extends UIComponentBase {

	private static String COMPONENT_TYPE = "com.xblog.SideBarItem";
	private static String RENDERER_TYPE = "com.xblog.SideBarItem";
	private static String FAMILY = "com.xblog";

	private String sideBarType;
	private String sideBarLocation;
	private String sideBarTitle;
	private String sideBarContent;
	private int sideBarArchiveCount;
	private String sideBarItemId;
	private int sideBarNumberItems;
	private String sideBarArchiveDateFormat;
	private String sideBarDocUNID;
	private String twitterBackgroundColor;
	private String twitterTweetColor;
	private String twitterLinkColor;
	private boolean showTwitterAvatar;
	private boolean showTwitterScrollbar;
	private String twitterSidebarWidth;
	private String twitterUser;
	private String pollQuestion;
	private String pollUniqueId;
	private String feedPortletUrl;

	private Map<String, String> pollChoice; //choice,alias

	public SideBarItem() {
		setRendererType(RENDERER_TYPE);
		setId(sideBarDocUNID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void encodeBegin(FacesContext context) throws IOException {
		super.encodeBegin(context);
		if (getSideBarType() != null && getSideBarType().equalsIgnoreCase("Tag Cloud")) {
			/**
			 * We have to build the tag cloud here as a child of the sideBarItem instead
			 * of in the renderer. We do it here because it's too late in the cycle if
			 * we do it in the renderer
			 */
			UITagCloud tagCloud = new UITagCloud();
			tagCloud.setSliderVisible(true);
			ViewTagCloudData cloudData = new ViewTagCloudData();
			cloudData.setViewName("(luTags)");
			cloudData.setCategoryColumn(0);
			cloudData.setLinkMetaSeparator("~");
			cloudData.setLinkRequestParam("tag");
			cloudData.setMaxTagLimit(25);
			cloudData.setLinkTargetPage("/default.xsp");
			cloudData.setCacheMode("off");
			tagCloud.setCloudData(cloudData);

			this.getChildren().add(buildDojoPortlet(tagCloud));
		} else if (getSideBarType() != null && getSideBarType().equalsIgnoreCase("Subscribe")) {
			try {
				//Build the HTML List
				String blogUrl = XBlogUtils.getCurrentInstance().getBlogUrl();
				UIList list = new UIList();
				list.setId("subscribeList1");
				list.setStyleClass("SidebarList SubscribeList");
				list.setItemStyleClass("SidebarListItem SubscribeListItem");

				XspOutputLink postsLink = new XspOutputLink();
				postsLink.setText(RenderUtils.getTranslationText("<$TransFullPosts$>"));
				postsLink.setTarget("_blank");
				postsLink.setTitle("Full Posts RSS");
				postsLink.setEscape(true);
				postsLink.setValue(blogUrl + "/NewsFeed.xsp");
				list.getChildren().add(postsLink);

				XspOutputLink commentLink = new XspOutputLink();
				commentLink.setText(RenderUtils.getTranslationText("<$TransSubscribeComments$>"));
				commentLink.setTarget("_blank");
				commentLink.setTitle("Comments RSS");
				commentLink.setEscape(true);
				commentLink.setValue(blogUrl + "/NewsFeed.xsp?type=comments");
				list.getChildren().add(commentLink);

				UIPanelEx portlet = buildDojoPortlet(list);
				
				XspSelectOneMenu combo = buildRSSCategoryCombo();
				portlet.getChildren().add(combo);
				
				this.getChildren().add(portlet);
			} catch (NotesException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public XspSelectOneMenu buildRSSCategoryCombo() throws NotesException {
		Database db = JSFUtil.getCurrentDatabase();
		View view = db.getView("(luTags)");
		ViewNavigator nav = view.createViewNav();
		ViewEntry ent = nav.getFirst();
		int counter = 0;
		XspSelectOneMenu combo = new XspSelectOneMenu();
		combo.setId("subscribeCombo1");
		combo.setOnchange("XBlog.subscribeToCategory(dojo.query(\"[id$='subscribeCombo1']\")[0].value);");
		while (ent != null) {
			UISelectItemEx selection = new UISelectItemEx();
			if (counter == 0) {
				selection.setItemLabel("Select an RSS Category");
				selection.setItemValue("Select an RSS Category");
			} else {
				String val = ent.getColumnValues().get(0).toString();
				selection.setItemLabel(val.substring(0, val.indexOf("~")));
				selection.setItemValue(val.substring(0, val.indexOf("~")));
			}
			selection.setId("selectCategory" + ((Integer) counter).toString());
			combo.getChildren().add(selection);
			counter++;
			ViewEntry prevEnt = ent;
			ent = nav.getNextSibling(ent);
			prevEnt.recycle();
		}
		return combo;
	}

	@SuppressWarnings("unchecked")
	public UIPanelEx buildDojoPortlet(UIComponent component) {
		UIPanelEx portlet = new UIPanelEx();
		portlet.setDojoType("dojox.widget.Portlet");
		List<DojoAttribute> dojoAttribs = new ArrayList();
		DojoAttribute titleAttrib = new DojoAttribute();
		titleAttrib.setName("title");
		titleAttrib.setValue(getSideBarTitle());
		dojoAttribs.add(titleAttrib);
		portlet.setDojoAttributes(dojoAttribs);
		portlet.getChildren().add(component);
		return portlet;
	}

	@Override
	public String getFamily() {
		return FAMILY;
	}

	public String getSideBarType() {
		return sideBarType;
	}

	public void setSideBarType(String sideBarType) {
		this.sideBarType = sideBarType;
	}

	public String getSideBarLocation() {
		return sideBarLocation;
	}

	public void setSideBarLocation(String sideBarLocation) {
		this.sideBarLocation = sideBarLocation;
	}

	public String getSideBarTitle() {
		return sideBarTitle;
	}

	public void setSideBarTitle(String sideBarTitle) {
		this.sideBarTitle = sideBarTitle;
	}

	public String getSideBarContent() {
		return sideBarContent;
	}

	public void setSideBarContent(String sideBarContent) {
		this.sideBarContent = sideBarContent;
	}

	public int getSideBarArchiveCount() {
		return sideBarArchiveCount;
	}

	public void setSideBarArchiveCount(int sideBarArchiveCount) {
		this.sideBarArchiveCount = sideBarArchiveCount;
	}

	public String getSideBarItemId() {
		return sideBarItemId;
	}

	public void setSideBarItemId(String sideBarItemId) {
		this.sideBarItemId = sideBarItemId;
	}

	public int getSideBarNumberItems() {
		return sideBarNumberItems;
	}

	public void setSideBarNumberItems(int sideBarNumberItems) {
		this.sideBarNumberItems = sideBarNumberItems;
	}

	public String getSideBarArchiveDateFormat() {
		return sideBarArchiveDateFormat;
	}

	public void setSideBarArchiveDateFormat(String sideBarArchiveDateFormat) {
		this.sideBarArchiveDateFormat = sideBarArchiveDateFormat;
	}

	public String getSideBarDocUNID() {
		return sideBarDocUNID;
	}

	public void setSideBarDocUNID(String sideBarDocUNID) {
		this.sideBarDocUNID = sideBarDocUNID;
	}

	@Override
	public void restoreState(FacesContext context, Object state) {
		// so we don't forget to include this in the future
		Object values[] = (Object[]) state;
		super.restoreState(context, values[0]);
		sideBarArchiveCount = (Integer) values[1];
		sideBarArchiveDateFormat = (String) values[2];
		sideBarContent = (String) values[3];
		sideBarDocUNID = (String) values[4];
		sideBarItemId = (String) values[5];
		sideBarLocation = (String) values[6];
		sideBarNumberItems = (Integer) values[7];
		sideBarTitle = (String) values[8];
		sideBarType = (String) values[9];
		feedPortletUrl = (String) values[10];
	}

	@Override
	public Object saveState(FacesContext context) {
		Object values[] = new Object[11];
		values[0] = super.saveState(context);
		values[1] = sideBarArchiveCount;
		values[2] = sideBarArchiveDateFormat;
		values[3] = sideBarContent;
		values[4] = sideBarDocUNID;
		values[5] = sideBarItemId;
		values[6] = sideBarLocation;
		values[7] = sideBarNumberItems;
		values[8] = sideBarTitle;
		values[9] = sideBarType;
		values[10] = feedPortletUrl;
		return values;
	}

	public String getTwitterBackgroundColor() {
		return twitterBackgroundColor;
	}

	public void setTwitterBackgroundColor(String twitterBackgroundColor) {
		this.twitterBackgroundColor = twitterBackgroundColor;
	}

	public String getTwitterTweetColor() {
		return twitterTweetColor;
	}

	public void setTwitterTweetColor(String twitterTweetColor) {
		this.twitterTweetColor = twitterTweetColor;
	}

	public String getTwitterLinkColor() {
		return twitterLinkColor;
	}

	public void setTwitterLinkColor(String twitterLinkColor) {
		this.twitterLinkColor = twitterLinkColor;
	}

	public boolean isShowTwitterAvatar() {
		return showTwitterAvatar;
	}

	public void setShowTwitterAvatar(boolean showTwitterAvatar) {
		this.showTwitterAvatar = showTwitterAvatar;
	}

	public boolean isShowTwitterScrollbar() {
		return showTwitterScrollbar;
	}

	public void setShowTwitterScrollbar(boolean showTwitterScrollbar) {
		this.showTwitterScrollbar = showTwitterScrollbar;
	}

	public String getTwitterSidebarWidth() {
		return twitterSidebarWidth;
	}

	public void setTwitterSidebarWidth(String twitterSidebarWidth) {
		this.twitterSidebarWidth = twitterSidebarWidth;
	}

	public String getTwitterUser() {
		return twitterUser;
	}

	public void setTwitterUser(String twitterUser) {
		this.twitterUser = twitterUser;
	}

	public String getPollQuestion() {
		return pollQuestion;
	}

	public void setPollQuestion(String pollQuestion) {
		this.pollQuestion = pollQuestion;
	}

	public Map<String, String> getPollChoice() {
		if (pollChoice == null) {
			pollChoice = new HashMap<String, String>();
		}
		return pollChoice;
	}

	public void setPollChoice(Map<String, String> pollChoice) {
		this.pollChoice = pollChoice;
	}

	public String getPollUniqueId() {
		return pollUniqueId;
	}

	public void setPollUniqueId(String pollUniqueId) {
		this.pollUniqueId = pollUniqueId;
	}

	public void setFeedPortletUrl(String feedPortletUrl) {
		this.feedPortletUrl = feedPortletUrl;
	}

	public String getFeedPortletUrl() {
		return feedPortletUrl;
	}
}
