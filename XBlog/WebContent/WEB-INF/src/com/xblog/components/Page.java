package com.xblog.components;

/**
 * Since a post and a page are virtually identical, this extends com.xblog.components.Post
 * and has hardly none of it's own properties and methods except those required to use
 * a different renderer
 * @author Keith Strickland
 *
 */
public class Page extends Post {

	private static String FAMILY = "com.xblog";
	private static String COMPONENT_TYPE = "com.xblog.Page";
	private static String RENDERER_TYPE = "com.xblog.Page";

	public Page() {
		setRendererType(RENDERER_TYPE);
	}

	@Override
	public String getFamily() {
		return FAMILY;
	}
}
