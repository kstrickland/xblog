package com.xblog.components;

import com.ibm.xsp.component.UIPanelEx;

public class BlogContainer extends UIPanelEx {

	private static final String RENDERER_TYPE = "com.xblog.BlogContainer";
	private static final String FAMILY = "com.xblog";
	private static final String COMPONENT_TYPE = "com.xblog.BlogContainer";
	
	public BlogContainer() {
		setRendererType(RENDERER_TYPE);
	}
	
	@Override
	public String getFamily() {
		return FAMILY;
	}
}
