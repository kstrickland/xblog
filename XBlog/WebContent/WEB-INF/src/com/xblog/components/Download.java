package com.xblog.components;

import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

import com.xblog.utils.XBlogUtils;

public class Download extends Archive {

	private static String FAMILY = "com.xblog";
	private static String COMPONENT_TYPE = "com.xblog.Download";
	private static String RENDERER_TYPE = "com.xblog.Download";

	private String defaultHtml;

	public Download() {
		setRendererType(RENDERER_TYPE);
	}

	@Override
	public String getDefaultHtml() {
		XBlogUtils xbUtils = XBlogUtils.getCurrentInstance();
		if (defaultHtml != null) {
			return defaultHtml;
		}
		ValueBinding vb = getValueBinding("defaultHtml");
		if (vb != null && vb.getValue(FacesContext.getCurrentInstance()) != null) {
			return (String) vb.getValue(FacesContext.getCurrentInstance());
		} else if (isPost()) {
			Object defaultHtml = xbUtils.getLayoutValue("DownloadLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		} else if (!isPost()) {
			Object defaultHtml = xbUtils.getLayoutValue("PageLayout");
			if (defaultHtml instanceof Vector) {
				return xbUtils.vectorToString((Vector) defaultHtml);
			} else {
				return defaultHtml.toString();
			}
		} else {
			return "<h2>No HTML found in the defined Layout Document. Please build your Layout in the Layout form, Entry Defaults tab!</h2>";
		}
	}

	@Override
	public Object saveState(FacesContext context) {
		Object values[] = new Object[2];
		int idx = 0;
		values[idx] = super.saveState(context);
		values[idx++] = this.defaultHtml;
		return values;
	}

	@Override
	public void restoreState(FacesContext context, Object state) {
		Object values[] = (Object[]) state;
		int idx = 0;
		super.restoreState(context, values[idx]);
		this.defaultHtml = (String) values[idx++];
	}
}
