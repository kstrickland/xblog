function getUserRoles() {
	var name:NotesName = session.createName(@UserName());
	var acl:NotesACL = session.getCurrentDatabase().getACL();
	var aclEnt:NotesACLEntry = acl.getEntry(name.getAbbreviated());
	if (aclEnt) {
		var userRoles:java.util.Vector = aclEnt.getRoles();
		return userRoles;
	}else{
		aclEnt = acl.getFirstEntry();
		while (aclEnt != null) {
			var aclName:NotesName = aclEnt.getNameObject();
			if (aclEnt.getUserType() == 4 || aclEnt.getUserType() == 3) {
				var members:java.util.Vector = getGroupMembers(aclName.getAbbreviated());
				if (members && members.contains(name.getCanonical())) {
					return aclEnt.getRoles();
				}
			}
			aclEnt = acl.getNextEntry();
		}
		return null;
	}
}

function getGroupMembers(groupName) {
	var nabDb:NotesDatabase = session.getDatabase("","names.nsf");
	var userView:NotesView = nabDb.getView("($Users)");
	var members:java.util.Vector = null;
	if (groupName) {
		var groupDoc:NotesDocument = userView.getDocumentByKey(groupName);
		if (groupDoc) {
			members = groupDoc.getItemValue("Members");
		}
	}
	return members;
}

function isAdminOnly() {
	var adminPageArr = ["comment","configuration","emoticon","hottag","hottext","keywords","language","layout","link","sidebar","stylesheet"];
	var adminOnly = false;
	for (var i=0;i < adminPageArr.length;i++) {
		var pageUrl = context.getUrl().toString();
		var pageName = pageUrl.substring(pageUrl.lastIndexOf(".nsf/") +5).toLowerCase();
		if (pageName.indexOf(adminPageArr[i]) > -1) {
			adminOnly = true;
			break;
		}
	} 
	return adminOnly;
}