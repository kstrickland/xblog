/*
 * This is the XBlog Singleton API. This makes up the client side
 * API for XBlog
 */
var XBlog = function() {
		
	var xblog = {
		frontEndRpc: function() {
			if (window.frontEndRPC) {
				return window.frontEndRPC;
			}else{
				return null;
			}
		},
		showHideComment: function(action) {
			var commentDialogId = dojo.query("[id$='commentDialog']")[0];
        	if (action == "show") {
            	XSP.openDialog(commentDialogId.id);           
        	}else if (action == "hide") {
            	XSP.closeDialog(commentDialogId.id);
        	}
		},
		validateComment: function() {
			var validationFields = dojo.query("[aria-invalid='true']");
        	if (validationFields.length > 0) {
            	return false;
        	}else{
        		console.log("getting comment field");
        		var commentField = dojo.query("textarea[id$=':Comment']");       	
        		var commentValue;
        		console.log("commentField = ",commentField);
            	if (commentField.length == 0) {
            		commentField = dojo.query("textarea[id$=':CommentOld']")[0];
            		commentValue = commentField.value;
            	}else{
            		commentField = dijit.byNode(commentField[0]);
            		commentValue = commentField.getValue();
            	}
        		
        		if (commentValue) {        			
        			if (this.validateWebAddress()) {
        				return true;
        			}else{
        				return false;
        			}
        		}else{
        			return false;
        		}
        	}
		},
		validateWebAddress: function() {
			var webDij = dijit.byId(dojo.query("input[id$='Website']")[0].id);
			console.log("validateWebAddress of ",webDij.getValue());
			if (webDij && webDij.getValue().length > 0) {
				var regExp = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
				if (!regExp.test(webDij.getValue())) {
					webDij.required = true;
					webDij.invalidMessage = "Website must be formatted like 'http://yourdomain.com or http://www.yourdomain.com'";
					webDij.regExp = "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$";
					return false;
				}else{
					return true;
				}
			}else{
				return true;
			}
		},
		insertEmoticon: function(textStr) {
			if (textStr) {
				var commentNode = dojo.query("[id$='CommentOld']")[0];
				if (commentNode) {
					var curValue = commentNode.value;
					var newValue = curValue + " " + textStr + " ";
					commentNode.value = newValue;
				}
			}
		},
		returnCookieVal: function(cookiename) {
			var cookieval = dojo.cookie(cookiename);
			if (cookieval) {
				return cookieval;
			}else{
				return "";
			}
		},
		populateCommentForm: function(dialogId) {
			var dialogDij = dijit.byId(dialogId);
			if (dialogDij) {
				var values = [];
				var fields = [];
				fields[0] = dijit.byId(dojo.query("input[id$='Name']")[0].id);
				fields[1] = dijit.byId(dojo.query("input[id$='Email']")[0].id);
				fields[2] = dijit.byId(dojo.query("input[id$='Website']")[0].id);
				fields[3] = dijit.byId(dojo.query("input[id$='RememberMe']")[0].id);
				values[0] = this.returnCookieVal("XBlogCommentName");
				values[1] = this.returnCookieVal("XBlogCommentEmail");
				values[2] = this.returnCookieVal("XBlogCommentWebsite");
				values[3] = this.returnCookieVal("XBlogCommentRemember");
				for (var i = 0;i < fields.length;i++) {
					if (fields[i] && values[i]) {
						if (!fields[i].getValue()) {
							fields[i].setAttribute("value",values[i]);
						}
					}
				}
			}
		},
		partialRefresh: function(id, type) {
			if (id) {
				if (type == "get") {
					XSP.partialRefreshGet(id,{});
				}else if (type == "post") {
					XSP.partialRefreshPost(id,{});	
				}
			}else{
				console.error("'partialRefresh' - Node with ID " + id + " not found");
			}
		},
		getRestUrl: function() {
			var urlFieldVal = dojo.query("[id$=':url']")[0].innerHTML;
			var blogUrl = urlFieldVal + "default.xsp?";
			var url = blogUrl + xblogMobileRest.extraArgs + "&type=BLOG"; 
			return url;
		},
		pingViaRpc: function() {
			backendRpc.keepAlive();
		},
		showHideReadMore: function(unid) {
			var showHideDiv = dojo.query("[id='" + unid + "']")[0];
			var showHideLink = dojo.query("[id='ShowHideLink-" + unid + "']")[0];
			var divStyle = dojo.style(showHideDiv,"display");
			if (divStyle.indexOf("none") > -1) {
				dojo.style(showHideDiv, "display", "block");
				dojo.replaceClass(showHideLink, "ShowHideCollapse", "ShowHideExpand");
			}else{
				dojo.style(showHideDiv, "display", "none");
				dojo.replaceClass(showHideLink, "ShowHideExpand", "ShowHideCollapse");
			}
		},
		getNsfUrl: function() {
			var url = location.href;
			var nsfUrl = "";
			if (url.indexOf(".nsf") == -1) {
				url = url.substring(0, url.lastIndexOf("/"));
				nsfUrl = url + dojo.query("[id$=':url']")[0].innerHTML;
				nsfUrl = nsfUrl.substring(0, nsfUrl.lastIndexOf("/"));
			}else{
				nsfUrl = url.substring(0,url.indexOf(".nsf")+4);
			}
			return nsfUrl;
		},
		getLogoutUrl: function() {
			var nsfUrl = this.getNsfUrl();
			var logoutUrl = nsfUrl + "?logout&redirectTo=" + nsfUrl + "/admin.xsp";
			return logoutUrl;
		},
		initCodeMirror: function(nodeElement, editorMode) {
			var codeMirror = CodeMirror.fromTextArea(nodeElement, {
				mode: editorMode,
				lineNumbers: true, 
				matchBrackets: true,
				extraKeys: {
					"F10": XBlog.codeMirrorFullScreen,
					"Esc": XBlog.codeMirrorExitFullScreen
				},
				onFocus: function() {
					console.log("onFocus for ",nodeElement.id);
					XBlog.codeMirrorInstances.currentCodeMirror = nodeElement.id;
				},
				onBlur: function() {
					console.log("onBlur for ",nodeElement.id);
					var currentCodeMirror = XBlog.codeMirrorInstances.currentCodeMirror;
					if (location.href.indexOf("layout.xsp") > -1) {
						XBlog.codeMirrorInstances[currentCodeMirror].toTextArea();
					}
					XBlog.codeMirrorInstances.currentCodeMirror = "";
				}
			});
			codeMirror.focus();
			XBlog.codeMirrorInstances[nodeElement.id] = codeMirror;
		},
		codeMirrorFullScreen: function() {
			var codeMirrorInstance = XBlog.codeMirrorInstances[XBlog.codeMirrorInstances.currentCodeMirror];
			var codeMirrorNode = codeMirrorInstance.getWrapperElement();
			console.log("codeMirrorNode=",codeMirrorNode);
			dojo.addClass(codeMirrorNode,"CodeMirrorFullScreen");
			codeMirrorInstance.refresh();
		},
		codeMirrorExitFullScreen: function() {
			var codeMirrorInstance = XBlog.codeMirrorInstances[XBlog.codeMirrorInstances.currentCodeMirror];
			var codeMirrorNode = codeMirrorInstance.getWrapperElement();
			console.log("codeMirrorNode=",codeMirrorNode);
			dojo.removeClass(codeMirrorNode,"CodeMirrorFullScreen");
			codeMirrorInstance.refresh();
		},
		codeMirrorInstances: {
			currentCodeMirror:""
		},
		subscribeToCategory: function(category) {
			var urlFieldVal = this.getNsfUrl();
			var feedUrl = urlFieldVal + "/NewsFeed.xsp?category=" + category;
			window.open(feedUrl);
		}
	}
	return xblog;
}();