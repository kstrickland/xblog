function buildLayoutPane(region, splitter) {
	var layoutPane = new com.ibm.xsp.extlib.component.dojo.layout.UIDojoContentPane();
	var attribList:java.util.List = new java.util.ArrayList();
	var regionDjAttrib = buildDojoAttribute("region",region)
	attribList.add(regionDjAttrib);
	if (splitter == true && region != "center") {
		var splitAttrib = buildDojoAttribute("splitter","true");
		attribList.add(splitAttrib); 
	}	
	if (region == "left" || region == "right") {
		if (XBlogUtils.getLayoutValue("type",0).toString().indexOf("3 Column-2") > -1) {
			layoutPane.setStyle("width: 47%;");
		}else{
			if (region == "left") {
				layoutPane.setStyle("width: 16%");
			}else{
				layoutPane.setStyle("width: 20%;");
			}
		}
		layoutPane.setStyleClass("DojoLayoutPane");
	}else{
		if (region == "center") {
			layoutPane.setStyle("overflow: auto;");
		}else{
			var centerLayoutPane = getComponent("xblogDjContentPanel");
			centerLayoutPane.setStyle("overflow: auto");
		}
		layoutPane.setStyleClass(region + "DojoLayoutPane")
	}
	layoutPane.setDojoAttributes(attribList);
	return layoutPane;
}

function buildDojoAttribute(name, value) {
	var dojoAttrib = new com.ibm.xsp.dojo.DojoAttribute();
	dojoAttrib.setName(name);
	dojoAttrib.setValue(value);
	return dojoAttrib;
}

function buildBorderContainer(region, splitter) {
	var borderContainer = new com.ibm.xsp.extlib.component.dojo.layout.UIDojoBorderContainer();
	var attribList:java.util.List = new java.util.ArrayList();
	var regionDjAttrib = buildDojoAttribute("region",region);
	attribList.add(regionDjAttrib);
	if (splitter == true) {
		var splitAttrib = buildDojoAttribute("splitter","true");
		attribList.add(splitAttrib); 
	}
	borderContainer.setStyle("width: 50%");
	borderContainer.setStyleClass("DojoSidebarContainer");
	borderContainer.setDojoAttributes(attribList);
	borderContainer.setStyle("width: 38%; height: 100%;");
	return borderContainer;
}

function buildSidebarContainer(side) {
	var sideBar = new com.ibm.xsp.component.UIPanelEx();
	var id = "Layout" + side + "SidePanel";
	var styleClass = side + "SideBarInner";
	sideBar.setId(id);
	sideBar.setStyleClass(styleClass);
	return sideBar;
}

function buildHeaderFooter(type) {
	var containerDiv = new com.ibm.xsp.component.xp.XspDiv();
	containerDiv.setStyleClass(type + "Inner");
	var outputTxt = new com.ibm.xsp.component.xp.XspOutputText();
	var defaultHtml = XBlogUtils.getLayoutValue(type + "layout");
	if (defaultHtml instanceof java.util.Vector) {
		defaultHtml = XBlogUtils.vectorToString(defaultHtml);
	}
	var html = com.xblog.renderkit.utils.RenderUtils.buildGenericHtml(defaultHtml,null);
	outputTxt.setContentType("HTML");
	outputTxt.setValue(html);
	containerDiv.getChildren().add(outputTxt);
	return containerDiv;
}

function buildGenericContent() {
	var outputTxt = new com.ibm.xsp.component.xp.XspOutputText();
	outputTxt.setValue("<p>Generic Content</p>");
	outputTxt.setContentType("HTML");
	return outputTxt;
}

function buildIncludePage() {
	var include = new com.ibm.xsp.component.UIIncludeComposite();
	include.setId("contentInner");
	var pageName;
	try {
		if (XBlogUtils.getConfigValue("SiteMaintainenance",0) == "Yes") {
			pageName = "downForMaintenance.xsp";
		}else{
			pageName = "partLayoutGeneric.xsp";
		}
	}catch (e) {
		pageName = "blank.xsp";
	}
	include.setPageName(pageName);
	return include;
}